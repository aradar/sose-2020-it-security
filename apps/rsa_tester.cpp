#include <bigint/BigInt.hpp>
#include <rsa/Rsa.hpp>
#include "rsa/RsaKeys.hpp"

int main() {
    auto pair = its::RsaKeyPair::generate(256);

    auto rsa = its::Rsa(pair);

    its::BigInt64 message = 32423345345;
    std::cout << "initially: " << message.to_string(10) << std::endl;

    message = rsa.encrypt(message);
    std::cout << "encrypted: " << message.to_string(10) << std::endl;

    message = rsa.decrypt(message);
    std::cout << "decrypted: " << message.to_string(10) << std::endl;

    return 0;
}
