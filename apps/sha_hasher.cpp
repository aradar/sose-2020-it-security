#include <iostream>
#include <cstring>
#include "sha/Sha256.hpp"

int main() {
    auto sha = its::Sha256();

    //std::string str = "abc";
    //std::string str = "abcdbcdecdefdefgefghfghighijhijkijkljklmklmnlmnomnopnopq";
    //sha.update(str.data(), str.size());

    // 1 char is 4 bits
    // 2 chars is 1 byte
    // num_chars / 2 without +0x is the num needed bytes

    std::string hex_str = "0x0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000";
    //std::string hex_str = "0xaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa";

    sha.hexupdate(hex_str);

    //std::string hex_str = "+0xbd";
    //std::string hex_str = "+0xb0";
    //uint16_t num = std::stoi(hex_str, nullptr, 16);
    //uint8_t array[sizeof(num)];
    //std::memcpy(array, &num, sizeof(num));
    //sha.update(reinterpret_cast<const char *>(array), 1);

    std::cout << sha.hexdigest() << std::endl;

    //std::cout << "this should be: ba7816bf8f01cfea414140de5dae2223b00361a396177a9cb410ff61f20015ad" << std::endl;
    //std::cout << "this should be: 248D6A61 D20638B8 E5C02693 0C3E6039 A33CE459 64FF2167 F6ECEDD4 19DB06C1" << std::endl;
    //std::cout << "this should be: 0xf616b0d54e78571a9611f343c9f8e022e859e920381ab0e4d3da01e193a7bd7e" << std::endl;
}
