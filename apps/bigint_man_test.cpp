#include "bigint/BigInt.hpp"

namespace bla {
    template<class T, class U>
    class Bli {
        T value;

        void test_func(std::vector<Bli<T, U>> bli);
    };
}

namespace bla {
    template<class T, class U>
    void Bli<T, U>::test_func(std::vector<Bli<T, U>> bli) {

    }
}

int main() {
    its::BigInt16 test(9223372036854775807);

    std::cout << test.to_string(10) << std::endl;


    std::vector<its::BigInt16> bla;
    test.is_fermat_prime(bla);

    its::BigInt16 prime_test(41);
    std::vector<uint64_t> div_bases{2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31};
    std::cout << std::boolalpha << prime_test.is_div_prime(div_bases) << std::endl;


}
