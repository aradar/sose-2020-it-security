#pragma clang diagnostic push
#pragma ide diagnostic ignored "cert-err58-cpp"

#include <set>
#include <deque>
#include "bigint/BigInt.hpp"

// this file contains the empirical test for task 2

#pragma region number definitions

template<class T>
std::set<T> merge_sets(std::vector<std::set<T>> sets) {
    std::set<T> merged;
    for (auto &s: sets) {
        merged.insert(s.begin(), s.end());
    }
    return merged;
}

std::vector<uint64_t> primes_below_100{
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41,
    43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97
};

std::vector<its::BigInt32> primes_up_to_37{
    2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37
};

// 200 primes sampled between 5000 and 100000
std::vector<its::BigInt32> rnd_primes{
    20029, 37501, 15377, 59771, 80209, 98221, 88423, 48533, 43997, 63611, 50423, 6271, 88657, 31039, 39139, 60859,
    12511, 27337, 60373, 25939, 41077, 44983, 66449, 16649, 5737, 86467, 58403, 88037, 87931, 25117, 17713, 24473,
    83833, 47491, 52127, 50671, 66959, 28837, 34129, 77513, 96827, 59443, 65719, 72901, 78691, 12953, 27917, 15271,
    97649, 23011, 66533, 10903, 29569, 79147, 72383, 47123, 64013, 13537, 48589, 83621, 63863, 82009, 66499, 89371,
    97003, 28933, 98801, 89431, 86413, 62897, 59513, 96269, 67213, 39839, 39313, 71807, 28751, 39671, 62983, 63131,
    53093, 25349, 77813, 55343, 60107, 25033, 63929, 17609, 65867, 38113, 29833, 39953, 36541, 5147, 10987, 72643,
    95747, 14759, 65423, 26561, 36551, 92221, 34549, 27779, 17053, 58771, 5387, 42989, 82787, 72223, 26881, 37607,
    9421, 77713, 7177, 92551, 55009, 20521, 20947, 51683, 85087, 42667, 78191, 58129, 50263, 99277, 17909, 7621,
    55639, 41953, 68261, 95873, 86209, 64927, 62201, 14489, 75307, 26497, 41887, 8623, 6791, 20509, 31123, 91253,
    81799, 22973, 21803, 8609, 95581, 36011, 24029, 26053, 88729, 23977, 36677, 24953, 64223, 80051, 45127, 12301,
    13537, 80449, 87317, 67211, 27299, 71887, 89431, 27919, 83903, 57487, 63391, 85427, 7879, 90697, 21647, 7937,
    27749, 5483, 85361, 44983, 50971, 23819, 18539, 49139, 91099, 9941, 95987, 74897, 27067, 35447, 10289, 18289,
    15391, 70229, 13049, 70139, 84967, 97571, 64499, 43331
};

// 200 no prime odd numbers sampled between 5000 and 100000
std::vector<its::BigInt32> rnd_no_primes{
    80743, 20439, 49377, 86061, 58781, 90445, 78231, 87663, 65879, 44343, 76737, 78085, 9051, 66315, 73223, 23383,
    56831, 58445, 61783, 99597, 17025, 21641, 39533, 88859, 32883, 71235, 40905, 53709, 93793, 8255, 48391, 62943,
    37851, 37125, 75645, 61235, 69807, 69357, 14473, 63355, 64087, 12171, 26765, 66095, 89667, 72413, 7893, 91517,
    37869, 63711, 37389, 90721, 88233, 65793, 63175, 82551, 88907, 52005, 90611, 31953, 41945, 55387, 13179, 6309,
    31927, 67695, 28053, 97125, 59643, 43851, 49675, 13765, 28269, 68553, 46839, 69385, 55835, 78287, 60949, 78333,
    13445, 69059, 13899, 57543, 36125, 78689, 86309, 87529, 7969, 94223, 29143, 50719, 78253, 65237, 26617, 71265,
    9179, 87687, 53159, 48517, 24599, 89479, 89053, 12995, 62663, 14547, 19585, 21791, 64691, 71095, 62139, 28601,
    84187, 9905, 90275, 16849, 84663, 24835, 84259, 65039, 18369, 46697, 77097, 11237, 95167, 98167, 44523, 61611,
    23577, 74839, 80181, 41883, 97797, 58791, 50941, 46583, 30343, 11147, 24217, 43695, 96037, 84561, 65049, 32033,
    9097, 16601, 39211, 47885, 6347, 37795, 20585, 65845, 67347, 74119, 35927, 52219, 77283, 41563, 29707, 88995,
    8917, 67591, 91975, 60417, 5917, 35487, 40051, 40403, 93247, 14465, 74371, 41495, 58695, 91749, 10241, 94729,
    92097, 97951, 58755, 59337, 75825, 38095, 77703, 81007, 45215, 43229, 98249, 34923, 25119, 33093, 94711, 52881,
    85225, 42939, 5161, 52337, 36207, 52131, 83707, 86433
};

std::set<its::BigInt32> abs_euler_pseudo_primes{
    1729, 2465, 15841, 46657, 62745, 63973, 75361, 101101, 126217, 162401, 172081, 188461, 278545, 340561, 399001,
    449065, 488881, 530881, 552721, 656601, 658801, 670033, 748657, 838201, 852841, 997633
};

std::set<its::BigInt32> carmichael_numbers{
    561, 1105, 1729, 2465, 2821, 6601, 8911, 10585, 15841, 29341, 41041, 46657, 52633, 62745, 63973, 75361, 101101,
    115921, 126217, 162401, 172081, 188461, 252601, 278545, 294409, 314821, 334153, 340561, 399001, 410041, 449065,
    488881, 512461, 530881, 552721, 656601, 658801, 670033, 748657, 825265, 838201, 852841, 997633, 1024651,
    1033669, 1050985, 1082809, 1152271, 1193221, 1461241, 1569457, 1615681, 1773289, 1857241, 1909001, 2100901,
    2113921, 2433601, 2455921, 2508013, 2531845, 2628073, 2704801, 3057601, 3146221, 3224065, 3581761, 3664585,
    3828001, 4335241, 4463641, 4767841, 4903921, 4909177, 5031181, 5049001, 5148001, 5310721, 5444489, 5481451,
    5632705, 5968873, 6049681, 6054985, 6189121, 6313681, 6733693, 6840001, 6868261, 7207201, 7519441, 7995169,
    8134561, 8341201, 8355841, 8719309, 8719921, 8830801, 8927101, 9439201, 9494101, 9582145, 9585541, 9613297,
    9890881, 10024561, 10267951, 10402561, 10606681, 10837321, 10877581, 11119105, 11205601, 11921001, 11972017,
    12261061, 12262321, 12490201, 12945745, 13187665, 13696033, 13992265, 14469841, 14676481, 14913991, 15247621,
    15403285, 15829633, 15888313, 16046641, 16778881, 17098369, 17236801, 17316001, 17586361, 17812081, 18162001,
    18307381, 18900973, 19384289, 19683001, 20964961, 21584305, 22665505, 23382529, 25603201, 26280073, 26474581,
    26719701, 26921089, 26932081, 27062101, 27336673, 27402481, 28787185, 29020321, 29111881, 31146661, 31405501,
    31692805, 32914441, 33596641, 34196401, 34657141, 34901461, 35571601, 35703361, 36121345, 36765901, 37167361,
    37280881, 37354465, 37964809, 38151361, 38624041, 38637361, 39353665, 40280065, 40430401, 40622401, 40917241
};

std::set<its::BigInt32> strong_pseudo_primes{
    // to base 2
    2047, 3277, 4033, 4681, 8321, 15841, 29341, 42799, 49141, 52633, 65281, 74665, 80581, 85489, 88357, 90751, 104653,
    130561, 196093, 220729, 233017, 252601, 253241, 256999, 271951, 280601, 314821, 357761, 390937, 458989, 476971,
    486737,
    // to base 3
    121, 703, 1891, 3281, 8401, 8911, 10585, 12403, 16531, 18721, 19345, 23521, 31621, 44287, 47197, 55969, 63139,
    74593, 79003, 82513, 87913, 88573, 97567, 105163, 111361, 112141, 148417, 152551, 182527, 188191, 211411, 218791,
    221761, 226801,
    // to base 4
    341, 1387, 2047, 3277, 4033, 4371, 4681, 5461, 8321, 8911, 10261, 13747, 14491, 15709, 15841, 19951, 29341, 31621,
    42799, 49141, 49981, 52633, 60787, 65077, 65281, 74665, 80581, 83333, 85489, 88357, 90751, 104653, 123251, 129921,
    130561, 137149,
    // to base 5
    781, 1541, 5461, 5611, 7813, 13021, 14981, 15751, 24211, 25351, 29539, 38081, 40501, 44801, 53971, 79381, 100651,
    102311, 104721, 112141, 121463, 133141, 141361, 146611, 195313, 211951, 216457, 222301, 251521, 289081, 290629,
    298271, 315121,
    // to base 6
    217, 481, 1111, 1261, 2701, 3589, 5713, 6533, 11041, 14701, 20017, 29341, 34441, 39493, 43621, 46657, 46873, 49141,
    49661, 58969, 74023, 74563, 76921, 83333, 87061, 92053, 94657, 94697, 97751, 97921, 109061, 115921, 125563, 128627,
    151387, 173377,
    // to base 7
    25, 325, 703, 2101, 2353, 4525, 11041, 14089, 20197, 29857, 29891, 39331, 49241, 58825, 64681, 76627, 78937, 79381,
    87673, 88399, 88831, 102943, 109061, 137257, 144901, 149171, 173951, 178709, 188191, 197633, 219781, 227767,
    231793, 245281,
    // to base 8
    9, 65, 481, 511, 1417, 2047, 2501, 3277, 3641, 4033, 4097, 4681, 8321, 11041, 15841, 16589, 19561, 24311, 24929,
    29341, 41441, 42799, 45761, 49141, 52429, 52633, 54161, 55969, 56033, 59291, 61337, 65281, 66197, 74023, 74665,
    77161, 80581, 85489, 87061,
    // to base 9
    91, 121, 671, 703, 1541, 1729, 1891, 2821, 3281, 3367, 3751, 5551, 7381, 8401, 8911, 10585, 11011, 12403, 14383,
    15203, 16471, 16531, 18721, 19345, 23521, 24661, 24727, 28009, 29341, 30857, 31621, 32791, 38503, 44287, 46999,
    47197, 49051, 49141, 53131

};

std::set<its::BigInt32> euler_pseudo_primes{
    65, 85, 91, 105, 121, 133, 145, 169, 185, 217, 247, 273, 301, 305, 325, 341, 343, 377, 385, 425, 435, 451, 481,
    511, 561, 585, 645, 657, 671, 703, 781, 793, 817, 841, 889, 905, 949, 985, 1001, 1099, 1105, 1111, 1225, 1233,
    1247, 1261, 1271, 1281, 1305, 1541, 1661, 1729, 1785, 1825, 1849, 1905, 2047, 2101, 2149, 2257, 2353, 2465
};

std::set<its::BigInt32> mr_composite_numbers{
    2047, 1373653, 25326001, 3215031751, 2152302898747, 3474749660383, 341550071728321, 341550071728321,
    3825123056546413051, 3825123056546413051, 3825123056546413051, its::BigInt32("318665857834031151167461"),
    its::BigInt32("3317044064679887385961981")
};

auto all_pseudo_primes = merge_sets<its::BigInt32>(std::vector<std::set<its::BigInt32>>{
        abs_euler_pseudo_primes, carmichael_numbers,
        strong_pseudo_primes, euler_pseudo_primes, mr_composite_numbers});

std::vector<its::BigInt32> pseudo_prime_candidates(all_pseudo_primes.begin(), all_pseudo_primes.end());

#pragma endregion

template<class T>
int execute_div_test(std::vector<T> &candidates) {
    std::vector<bool> results;
    results.reserve(candidates.size());

    for (auto &curr_can: candidates) {
        results.push_back(curr_can.is_div_prime(primes_below_100));
    }

    return std::accumulate(results.begin(), results.end(), 0);
}

template<class T>
std::vector<T> generate_random_bases(T &prime, int num_bases) {
    T lower_bound = 2;
    T upper_bound(prime);
    upper_bound.sub(lower_bound);

    std::vector<T> bases;
    bases.reserve(num_bases);
    for (int i = 0; i < num_bases; ++i) {
        bases.push_back(T::generate_random(lower_bound, upper_bound));
    }

    return bases;
}

template<class T>
std::tuple<double, double, double, double> execute_test_block(
        std::vector<T> &candidates,
        int gen_num_bases = 0,
        std::vector<T> fixed_bases = {}) {

    auto num_runs = 1;
    if (gen_num_bases > 0) {
        num_runs = 100;
    }

    std::vector<bool> fermat_1_results;
    fermat_1_results.reserve(candidates.size() * num_runs);
    std::vector<bool> fermat_2_results;
    fermat_2_results.reserve(candidates.size() * num_runs);
    std::vector<bool> euler_results;
    euler_results.reserve(candidates.size() * num_runs);
    std::vector<bool> mr_results;
    mr_results.reserve(candidates.size() * num_runs);

    for (int i = 0; i < num_runs; ++i) {
        for (auto &curr_can: candidates) {
            std::vector<T> bases(fixed_bases);
            std::vector<T> rng_bases = generate_random_bases<T>(curr_can, gen_num_bases);
            bases.insert(bases.end(), rng_bases.begin(), rng_bases.end());

            fermat_1_results.push_back(curr_can.is_fermat_prime(bases, false));
            fermat_2_results.push_back(curr_can.is_fermat_prime(bases, true));
            euler_results.push_back(curr_can.is_euler_prime(bases));
            mr_results.push_back(curr_can.is_mr_prime(bases));
        }
    }

    auto fermat_1_sum = std::accumulate(
            fermat_1_results.begin(), fermat_1_results.end(), 0) / static_cast<double>(num_runs);
    auto fermat_2_sum = std::accumulate(
            fermat_2_results.begin(), fermat_2_results.end(), 0) / static_cast<double>(num_runs);
    auto euler_sum = std::accumulate(
            euler_results.begin(), euler_results.end(), 0) / static_cast<double>(num_runs);
    auto mr_sum = std::accumulate(mr_results.begin(), mr_results.end(), 0) / static_cast<double>(num_runs);

    return std::tuple<double, double, double, double>{fermat_1_sum, fermat_2_sum, euler_sum, mr_sum};
}

template<class T>
double execute_mr_test_block(
        std::vector<T> &candidates,
        int gen_num_bases) {

    const auto num_runs = 100;

    std::vector<bool> results;
    results.reserve(candidates.size() * num_runs);

    for (int i = 0; i < num_runs; ++i) {
        for (auto &curr_can: candidates) {
            std::vector<T> bases = generate_random_bases<T>(curr_can, gen_num_bases);
            results.push_back(curr_can.is_mr_prime(bases));
        }
    }

    double sum = std::accumulate(results.begin(), results.end(), 0) / static_cast<double>(num_runs);

    return sum;
}

std::vector<std::vector<double>> mr_round_block(std::vector<int> &sample_points) {
    std::vector<std::vector<double>> results(3);

    for (auto num_bases: sample_points) {
        auto primes_result = execute_mr_test_block<its::BigInt32>(rnd_primes, num_bases);
        auto pseudo_primes_result = execute_mr_test_block<its::BigInt32>(pseudo_prime_candidates, num_bases);
        auto no_primes_result = execute_mr_test_block<its::BigInt32>(rnd_no_primes, num_bases);

        results[0].push_back(primes_result / static_cast<double>(rnd_primes.size()));
        results[1].push_back(1 - pseudo_primes_result / static_cast<double>(pseudo_prime_candidates.size()));
        results[2].push_back(1 - no_primes_result / static_cast<double>(rnd_no_primes.size()));
    }

    return results;
}

template<class T>
void run_accuracy_test(
        std::vector<T> &candidates,
        int gen_num_bases,
        bool reverse_acc = false,
        std::vector<T> fixed_bases = {}) {

    auto results = execute_test_block<T>(candidates, gen_num_bases, fixed_bases);

    double divisor = candidates.size();

    if (!reverse_acc) {
        std::cout << "fermat test (no gcd variant) accuracy: "
            << (std::get<0>(results) / divisor) << std::endl;
        std::cout << "fermat test (with gcd variant) accuracy: "
            << (std::get<1>(results) / divisor) << std::endl;
        std::cout << "euler test accuracy: "
            << (std::get<2>(results) / divisor) << std::endl;
        std::cout << "miller rabin test (no gcd variant) accuracy: "
            << (std::get<3>(results) / divisor) << std::endl;
    } else {
        std::cout << "fermat test (no gcd variant) accuracy: "
            << (1 - std::get<0>(results) / divisor) << std::endl;
        std::cout << "fermat test (with gcd variant) accuracy: "
            << (1 - std::get<1>(results) / divisor) << std::endl;
        std::cout << "euler test accuracy: "
            << (1 - std::get<2>(results) / divisor) << std::endl;
        std::cout << "miller rabin test (no gcd variant) accuracy: "
            << (1 - std::get<3>(results) / divisor) << std::endl;
    }
}

template<class T>
void accuracy_test_block(
        std::vector<T> &candidates,
        const char *text,
        bool reverse_acc = false) {

    std::cout << "testing accuracy with " << candidates.size() << " " << text << "" << std::endl;
    std::cout << std::string(80, '-') << std::endl;
    auto div_test_result = execute_div_test(candidates) / static_cast<float>(candidates.size());

    if (reverse_acc) {
        div_test_result = 1 - div_test_result;
    }

    std::cout << std::endl
            << "div test accuracy with primes below 100: " << div_test_result << std::endl;

    std::cout << std::endl << "bases == all primes up to 37" << std::endl;
    run_accuracy_test<T>(candidates, 0, reverse_acc, primes_up_to_37);

    std::cout << std::endl << "bases == 20 random numbers between 2 and candidate - 2" << std::endl;
    run_accuracy_test<T>(candidates, 20, reverse_acc);

    std::cout << std::endl << "bases == the previous two combined" << std::endl;
    run_accuracy_test<T>(candidates, 20, reverse_acc, primes_up_to_37);
}

void evaluate_prime_test_accuracy() {
    accuracy_test_block<its::BigInt32>(
            rnd_primes, "random true prime numbers (task a)");
    std::cout << std::endl;

    accuracy_test_block<its::BigInt32>(
            pseudo_prime_candidates, "pseudo prime numbers (task b)", true);

    std::cout << std::endl;
    accuracy_test_block<its::BigInt32>(
            rnd_no_primes,
            "random non prime numbers (task c)",
            true);
}

void evaluate_mr_round_accuracy() {
    const auto col_width = 10;
    const auto first_col_width = 6;

    std::vector<int> sample_points{1, 10, 19, 29, 39, 50};
    auto results = mr_round_block(sample_points);

    std::cout << std::setw(first_col_width) << std::setfill(' ') << "Rounds";
    for (auto point: sample_points) {
        std::cout << std::setw(col_width) << std::setfill(' ') << point;
    }
    std::cout << std::endl << std::string(first_col_width + col_width * sample_points.size(), '-') << std::endl;

    std::deque<std::string> task_chars{"a)", "b)", "c)"};
    for (auto &row: results) {
        std::cout << std::setw(first_col_width) << std::setfill(' ') << (task_chars.front());
        task_chars.pop_front();
        for (auto &col: row) {
            std::cout << std::setw(col_width) << std::setfill(' ') << col;
        }
        std::cout << std::endl;
    }
}

int main() {
    std::cout << std::string(80, '+') << std::endl;
    std::cout << "empirical answer for the first question" << std::endl;
    std::cout << std::string(80, '+') << std::endl << std::endl;
    evaluate_prime_test_accuracy();

    std::cout << std::endl << std::string(80, '+') << std::endl;
    std::cout << "empirical answer for the second question" << std::endl;
    std::cout << std::string(80, '+') << std::endl << std::endl;
    evaluate_mr_round_accuracy();
    return 0;
}

#pragma clang diagnostic pop