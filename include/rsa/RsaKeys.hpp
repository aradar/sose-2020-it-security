#pragma once

#include <random>
#include <bigint/BigInt.hpp>

namespace its {
    struct RsaPublicKey {
        its::BigInt64 e;
        its::BigInt64 n;
    };

    struct RsaPrivateKey {
        its::BigInt64 d;
        its::BigInt64 n;
        its::BigInt64 p;
        its::BigInt64 q;
        its::BigInt64 phi;
    };

    struct RsaKeyPair {
        RsaPublicKey public_key;
        RsaPrivateKey private_key;

        /**
         * Generates a RSA Keypair with a module of at least size bits or a few bits more.
         * @param size number of bits the module should have.
         * @return generated key pair.
         */
        static RsaKeyPair generate(int size);

        /**
         * Generates a RSA keypair with the given primes numbers p and q.
         * @param p first prime number.
         * @param q second prime number.
         * @return generated key pair.
         */
        static RsaKeyPair generate(its::BigInt64 &p, its::BigInt64 &q);

        /**
         * Generates a RSA keypair with the given primes p and q as well as the public key
         * secret portion e.
         * @param p first prime number.
         * @param q second prime number.
         * @param e public key secret.
         * @return  generated key pair.
         */
        static RsaKeyPair generate(its::BigInt64 &p, its::BigInt64 &q, its::BigInt64 &e);
    };

    RsaKeyPair RsaKeyPair::generate(int size) {
        if (size < 64) {
            throw std::runtime_error("A RSA key must be at least 64 bits long!");
        }

        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> distr(0, 15);

        auto p = its::BigInt64::generate_random_prime(size / 2 + distr(gen));
        auto q = its::BigInt64::generate_random_prime(size / 2 + distr(gen));

        return RsaKeyPair::generate(p, q);
    }

    RsaKeyPair RsaKeyPair::generate(its::BigInt64 &p, its::BigInt64 &q) {
        its::BigInt64 e_lower_bound = 0xffffff; // 2^24
        its::BigInt64 one = 1;

        RsaPublicKey pub;
        RsaPrivateKey pri;

        pri.p = p;
        pri.q = q;

        pri.p.mul(pri.q, pri.n);
        pub.n = pri.n;

        its::BigInt64 p_minus_one, q_minus_one;
        pri.p.sub(one, p_minus_one);
        pri.q.sub(one, q_minus_one);
        p_minus_one.mul(q_minus_one, pri.phi);

        its::BigInt64 gcd;
        its::BigInt64 phi_minus_one;
        pri.phi.sub(one, phi_minus_one);
        do {
            // todo: shouldn't this be a prime num?
            pub.e = its::BigInt64::generate_random(e_lower_bound, phi_minus_one);
            gcd.gcd_bin(pub.e, pri.phi, gcd);
        } while (gcd != 1);

        pub.e.calc_inverse_element(pri.phi, pri.d);

        RsaKeyPair pair{pub, pri};
        return pair;
    }

    RsaKeyPair RsaKeyPair::generate(its::BigInt64 &p, its::BigInt64 &q, its::BigInt64 &e) {
        RsaPublicKey pub;
        RsaPrivateKey pri;

        pri.p = p;
        pri.q = q;

        p.mul(q, pri.n);
        pub.n = pri.n;

        its::BigInt64 one = 1;
        its::BigInt64 p_minus_one, q_minus_one;
        p.sub(one, p_minus_one);
        q.sub(one, q_minus_one);
        p_minus_one.mul(q_minus_one, pri.phi);

        pub.e = e;
        e.calc_inverse_element(pri.phi, pri.d);

        RsaKeyPair pair{pub, pri};
        return pair;
    }
}