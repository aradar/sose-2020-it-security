#pragma once

#include "RsaKeys.hpp"

namespace its {
    class Rsa {

    private:
        its::RsaKeyPair keys;

        static its::BigInt64 de_or_encrypt(
                its::BigInt64 &message,
                its::BigInt64 &key,
                its::BigInt64 &module) {

            its::BigInt64 one = 1, n_minus_one;
            module.sub(one, n_minus_one);

            if (message > n_minus_one) {
                throw std::runtime_error(
                        "A message must always be smaller than the used module (n)!");
            }

            if (message == 0 || message == 1 || message == n_minus_one) {
                throw std::runtime_error(
                        "The message you are trying to encrypt has the value of "
                        "0, 1 or n -1! Those values are not allowed as they are "
                        "effectively not encrypted.");
            }

            its::BigInt64 translated_message;
            message.pow_mod(message, key, module, translated_message);
            return translated_message;
        }

    public:
        Rsa(its::RsaKeyPair &keys): keys(keys) {}

        /**
         * Encrypts the given message with the help of the internally stored public key.
         * @param message the message to encrypt.
         * @return encrypted message.
         */
        its::BigInt64 encrypt(its::BigInt64 &message) {
            return de_or_encrypt(message, keys.public_key.e, keys.public_key.n);
        }

        /**
         * Decrypts the given encrypted message with the help of the internally stored private key.
         * @param encrypted_message the message to get decrypted.
         * @return decrypted message.
         */
        its::BigInt64 decrypt(its::BigInt64 &encrypted_message) {
            return de_or_encrypt(encrypted_message, keys.private_key.d, keys.private_key.n);
        }
    };
}

