#pragma once

namespace its {
    template<class TFull, class THalf>
    BigInt <TFull, THalf> BigInt<TFull, THalf>::generate_random_prime(size_t num_bits, bool enforce_highest_bit) {
        const int num_bases = 20;
        BigInt <TFull, THalf> prime;

        std::vector <BigInt<TFull, THalf>> bases;
        bases.reserve(num_bases);
        //for (int i = 0; i < num_bases; ++i) {
        //    bases.push_back(its::BigInt<TFull, THalf>::generate_random(num_bits / 2));
        //}

        std::vector <uint64_t> primes_below_100{
            2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};

        //std::vector <BigInt<TFull, THalf>> prime_bases_test{
        //        2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97};

        bool prime_found = false;
        do {
            prime = BigInt<TFull, THalf>::generate_random_odd(num_bits, enforce_highest_bit);

            //if (!prime.is_div_prime(primes_below_100)) {
            //    continue;
            //}

            bases.clear();
            BigInt <TFull, THalf> lower_bound = 2;
            BigInt <TFull, THalf> upper_bound(prime);
            //BigInt <TFull, THalf> upper_bound(prime);
            upper_bound.sub(lower_bound);
            for (int i = 0; i < num_bases; ++i) {
                bases.push_back(its::BigInt<TFull, THalf>::generate_random(lower_bound, upper_bound));
            }
            prime_found = prime.is_mr_prime(bases);
        } while (!prime_found);

        return prime;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::egcd_bin(
            its::BigInt<TFull, THalf> &first_num,
            its::BigInt<TFull, THalf> &second_num,
            its::BigInt<TFull, THalf> &gcd_dest,
            its::BigInt<TFull, THalf> &u_dest,
            its::BigInt<TFull, THalf> &v_dest) {

        // todo: this doesn't work even though it is exactly the same as on
        //  the slides. As a workaround the normal egcd has been added below.

        if (first_num.is_even() && second_num.is_even()) {
            throw std::runtime_error("egcd_bin only works if at least one of the numbers is odd!");
        }

        gcd_dest.clear();
        u_dest.clear();
        v_dest.clear();

        if (first_num.cells.is_empty() && second_num.cells.is_empty()) {
            return;
        }

        auto a(first_num);
        auto b(second_num);

        // force copies to be positive
        a.positive = true;
        b.positive = true;

        auto k = std::min(
                a.cells.cnt_trailing_zero_bits(),
                b.cells.cnt_trailing_zero_bits());
        a.right_shift(k);
        b.right_shift(k);

        auto x(a);
        auto y(b);

        BigInt <TFull, THalf> au = 1, av = 0, bu = 0, bv = 1;

        while (!x.cells.is_empty()) {
            while (x.is_even()) {
                x.right_shift(1);
                if (au.is_even() && av.is_even()) {
                    au.right_shift(1);
                    av.right_shift(1);
                } else {
                    au.add(b);
                    au.right_shift(1);
                    av.sub(a);
                    av.right_shift(1);
                }
            }

            while (y.is_even()) {
                y.right_shift(1);
                if (bu.is_even() && bv.is_even()) {
                    bu.right_shift(1);
                    bv.right_shift(1);
                } else {
                    //bu.add(a);
                    bu.add(b);
                    bu.right_shift(1);
                    //bv.sub(b);
                    bv.sub(a);
                    bv.right_shift(1);
                }
            }

            if (x < y) {
                y.sub(x);
                bu.sub(au);
                bv.sub(av);
            } else {
                x.sub(y);
                au.sub(bu);
                av.sub(bv);
            }
        }

        y.left_shift(k);
        gcd_dest.set(y);
        u_dest.set(bu);
        v_dest.set(bv);
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::egcd(
            its::BigInt<TFull, THalf> &first_num,
            its::BigInt<TFull, THalf> &second_num,
            its::BigInt<TFull, THalf> &gcd_dest,
            its::BigInt<TFull, THalf> &u_dest,
            its::BigInt<TFull, THalf> &v_dest) {

        gcd_dest.clear();
        u_dest.clear();
        v_dest.clear();

        if (first_num.cells.is_empty() && second_num.cells.is_empty()) {
            return;
        }

        auto a(first_num);
        auto b(second_num);

        BigInt <TFull, THalf> temp, q, u = 1, v = 0, s = 0, t = 1;

        while (!b.cells.is_empty()) {
            a.div(b, q);

            q.mul(b, temp);
            a.sub(temp, temp);
            a.set(b);
            b.set(temp);

            q.mul(s, temp);
            u.sub(temp, temp);
            u.set(s);
            s.set(temp);

            q.mul(t, temp);
            v.sub(temp, temp);
            v.set(t);
            t.set(temp);
        }

        gcd_dest.set(a);
        u_dest.set(u);
        v_dest.set(v);
    }
}

