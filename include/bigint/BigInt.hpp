#pragma once

#include <cstdint>
#include <cmath>
#include <string>
#include <algorithm>
#include <stdexcept>
#include <ios>
#include <sstream>
#include <iostream>
#include <random>
#include <vector>

#include "CellStorage.hpp"

namespace its {

    enum SimplePrimeResult {
        yes, no, maybe
    };

    /**
     * Generic BigInt class which can be used with different cell datatypes and
     * allows arbitrary precision int arithmetic.
     * @tparam TFull Dtype used for the full cell should be twice as big as THalf
     * @tparam THalf Dtype used for cell split should be half the size of TFull
     */
    template<class TFull, class THalf>
    class BigInt {
    private:
        THalf base;
        bool positive;
        CellStorage<TFull, THalf> cells;

        /**
         * Internal helper function which inits the properties based on the given
         * string number representation.
         * @param value std::string representing the arbitrary long integer number
         */
        void from_string(const std::string &value);

        /**
         * Internal helper function to safely init a BigInt instance through a
         * 64 bit int literal.
         * @param value initial value of the BigInt instance.
         * @return value splitted in even THalf sized chunks.
         */
        std::vector<THalf> int_to_cell_like(long long int value);

        void add_or_sub_cells(
                BigInt<TFull, THalf> &first,
                BigInt<TFull, THalf> &second,
                BigInt<TFull, THalf> &dest,
                bool add_numbers,
                bool dest_positive);

        void direct_cell_add(
                size_t index,
                Cell<TFull, THalf> value);

        void mul_cells(
                BigInt<TFull, THalf> &multiplier,
                BigInt<TFull, THalf> &multiplicand,
                BigInt<TFull, THalf> &dest,
                bool dest_positive);

        void mul_cells(
                BigInt<TFull, THalf> &multiplier,
                Cell<TFull, THalf> &multiplicand,
                BigInt<TFull, THalf> &dest);

        Cell<TFull, THalf> estimate_cell_div(
                Cell<TFull, THalf> upper,
                Cell<TFull, THalf> lower,
                Cell<TFull, THalf> divisor);

        void fix_estimation_slow(
                BigInt<TFull, THalf> &tmp,
                BigInt<TFull, THalf> &divisor,
                BigInt<TFull, THalf> &remainder,
                Cell<TFull, THalf> &estimate);

        void fix_estimation(
                BigInt<TFull, THalf> &tmp,
                BigInt<TFull, THalf> &divisor,
                BigInt<TFull, THalf> &remainder,
                Cell<TFull, THalf> &estimate);

        void fix_to_big_estimation(
                BigInt<TFull, THalf> &tmp,
                BigInt<TFull, THalf> &divisor,
                BigInt<TFull, THalf> &remainder,
                Cell<TFull, THalf> &estimate);

        void fix_to_small_estimation(
                BigInt<TFull, THalf> &tmp,
                BigInt<TFull, THalf> &divisor,
                BigInt<TFull, THalf> &remainder,
                Cell<TFull, THalf> &estimate);

        void moddiv_cells(
                BigInt<TFull, THalf> &dividend,
                BigInt<TFull, THalf> &divisor,
                BigInt<TFull, THalf> &dest,
                BigInt<TFull, THalf> &remainder,
                bool dest_positive);

        void double_cell_moddiv(
                BigInt<TFull, THalf> &dividend,
                BigInt<TFull, THalf> &divisor,
                BigInt<TFull, THalf> &dest,
                BigInt<TFull, THalf> &remainder);

        void cell_storage_moddiv(
                BigInt<TFull, THalf> &dividend,
                BigInt<TFull, THalf> &divisor,
                BigInt<TFull, THalf> &dest,
                BigInt<TFull, THalf> &remainder);

#pragma region task 2 extension

        its::SimplePrimeResult basic_prime_check();

#pragma endregion

    public:

        /**
         * Default constructor which initializes the fields like a "0" input would
         * do.
         **/
        explicit BigInt()
                : cells(),
                  positive(true),
                  base(~THalf{0}) {}

        /**
         * A constructor which accepts its initial value through a string
         * representative. Currently supported are strings with or without a sign
         * char (+/ -) and chars for the bases 8, 10 and 16. Strings which
         * represent numbers of base 8 and 16 need to be started with a identifier
         * (0o and 0x).
         **/
        explicit BigInt(const std::string &value)
                : cells(),
                  positive(true),
                  base(~THalf{0}) {

            from_string(value);
        }

#pragma clang diagnostic push
#pragma ide diagnostic ignored "google-explicit-constructor"

        BigInt(long long int value)
                : cells(int_to_cell_like(value)),
                  positive(value >= 0),
                  base(~THalf{0}) {}

#pragma clang diagnostic pop

        /**
         * Copy constructor which creates a copy of the given instance.
         **/
        BigInt(const BigInt<TFull, THalf> &bigint)
                : cells(bigint.cells),
                  positive(bigint.positive),
                  base(bigint.base) {}

        /**
         * Arithmetic add which adds the given second value on to this instance.
         * @param second value which gets added to the value of this instance.
         */
        void add(BigInt<TFull, THalf> &second) {
            add(*this, second, *this);
        }

        /**
         * Arithmetic add which adds the given second value on to this instance but
         * saves the output to the given dest instance.
         * @param second value which gets added to the value of this instance.
         * @param dest instance in which the result gets written.
         */
        void add(BigInt<TFull, THalf> &second,
                 BigInt<TFull, THalf> &dest) {
            add(*this, second, dest);
        }

        /**
         * Arithmetic add which adds the given first value on to the second and
         * writes the result into dest.
         * @param first value which gets added to the second value.
         * @param second value which gets added to the value of this instance.
         * @param dest instance in which the result gets written.
         */
        void add(BigInt<TFull, THalf> &first,
                 BigInt<TFull, THalf> &second,
                 BigInt<TFull, THalf> &dest);

        /**
         * Arithmetic subtraction which subtracts the given second value from this
         * instance and writes the output into this instance.
         * @param second value which gets subtracted from this instance.
         */
        void sub(BigInt<TFull, THalf> &second) {
            sub(*this, second, *this);
        }

        /**
         * Arithmetic subtraction which subtracts the given second value from this
         * instance and writes the output into the dest instance.
         * @param second value which gets subtracted from this instance.
         * @param dest instance in which the result gets written.
         */
        void sub(BigInt<TFull, THalf> &second,
                 BigInt<TFull, THalf> &dest) {
            sub(*this, second, dest);
        }

        /**
         * Arithmetic subtraction which subtracts the given second value from the
         * first and writes the output into the dest instance.
         * @param first base value.
         * @param second value which gets subtracted from first.
         * @param dest instance in which the result gets written.
         */
        void sub(BigInt<TFull, THalf> &first,
                 BigInt<TFull, THalf> &second,
                 BigInt<TFull, THalf> &dest);

        /**
         * Arithmetic multiplication which multiplies this instance with the
         * multiplicand and writes the output into this. Doing so creates
         * internally a copy of this instance.
         * @param multiplicand value with which this instance gets multiplied.
         */
        void mul(BigInt<TFull, THalf> &multiplicand) {
            BigInt<TFull, THalf> multiplier(*this);
            mul(multiplier, multiplicand, *this);
        }

        /**
         * Arithmetic multiplication which multiplies this instance with the
         * multiplicand and writes the output into dest. Doing so creates
         * internally a copy of this instance.
         * @param multiplier value with which this instance gets multiplied.
         * @param dest instance in which the result gets written.
         */
        void mul(BigInt<TFull, THalf> &multiplier,
                 BigInt<TFull, THalf> &dest) {
            mul(*this, multiplier, dest);
        }

        /**
         * Arithmetic multiplication which multiplies the instance of multiplier
         * with the multiplicand and writes the output into dest. Doing so creates
         * internally a copy of this instance.
         * @param multiplier base value which gets multiplied.
         * @param multiplicand value with which this instance gets multiplied.
         * @param dest instance in which the result gets written.
         */
        void mul(BigInt<TFull, THalf> &multiplier,
                 BigInt<TFull, THalf> &multiplicand,
                 BigInt<TFull, THalf> &dest);

        /**
         * Executes a division which returns the result and the remainder.
         * @param dividend number which gets divided.
         * @param divisor number which divides the dividend.
         * @param dest number in which the result gets written.
         * @param remainder number in which the remainder gets written.
         * @param round_dest defines if the output should get rounded.
         */
        void divmod(BigInt<TFull, THalf> &dividend,
                    BigInt<TFull, THalf> &divisor,
                    BigInt<TFull, THalf> &dest,
                    BigInt<TFull, THalf> &remainder,
                    bool round_dest = true);

        /**
         * Executes a division which returns the result and the remainder.
         * @param divisor number which divides the dividend.
         * @param dest number in which the result gets written.
         * @param remainder number in which the remainder gets written.
         * @param round_dest defines if the output should get rounded.
         */
        void divmod(BigInt<TFull, THalf> &divisor,
                    BigInt<TFull, THalf> &dest,
                    BigInt<TFull, THalf> &remainder,
                    bool round_dest = true) {

            divmod(*this, divisor, dest, remainder, round_dest);
        }

        /**
         * Executes a division which returns the result and the remainder.
         * @param divisor number which divides the dividend.
         * @param remainder number in which the remainder gets written.
         * @param round_dest defines if the output should get rounded.
         */
        void divmod(BigInt<TFull, THalf> &divisor,
                    BigInt<TFull, THalf> &remainder,
                    bool round_dest = true) {

            BigInt<TFull, THalf> dividend(*this);
            divmod(dividend, divisor, *this, remainder, round_dest);
        }

        /**
         * Divides the dividend with the divisor and safes the output in dest.
         * @param dividend number which gets divided.
         * @param divisor number which divides the dividend.
         * @param dest destination for the result.
         * @param round_dest defines if rounding should get applied.
         */
        void div(BigInt<TFull, THalf> &dividend,
                 BigInt<TFull, THalf> &divisor,
                 BigInt<TFull, THalf> &dest,
                 bool round_dest = true) {

            BigInt<TFull, THalf> remainder;
            divmod(dividend, divisor, remainder, round_dest);
        }

        /**
         * Divides this instance with the divisor and safes the output in dest.
         * @param divisor number which divides the dividend.
         * @param dest destination for the result.
         * @param round_dest defines if rounding should get applied.
         */
        void div(BigInt<TFull, THalf> &divisor,
                 BigInt<TFull, THalf> &dest,
                 bool round_dest = true) {

            BigInt<TFull, THalf> remainder;
            divmod(*this, divisor, dest, remainder, round_dest);
        }

        /**
         * Divides this instance with the divisor and safes the output in this
         * instance.
         * @param divisor number which divides the dividend.
         * @param round_dest defines if rounding should get applied.
         */
        void div(BigInt<TFull, THalf> &divisor,
                 bool round_dest = true) {

            BigInt<TFull, THalf> remainder;
            BigInt<TFull, THalf> dividend(*this);
            divmod(dividend, divisor, *this, remainder, round_dest);
        }

        /**
         * Arithmetic multiplication of this instance with the value 8. This is
         * internally implemented completely through shifting or at least
         * partially and is therefor faster than the generic mul.
         */
        void mul_8() {
            left_shift(2);
        }

        /**
         * Arithmetic multiplication of this instance with the value 10. This is
         * internally implemented completely through shifting or at least
         * partially and is therefor faster than the generic mul.
         */
        void mul_10() {
            BigInt<TFull, THalf> orig(*this);
            left_shift(2);
            add(orig);
            left_shift(1);
        }

        /**
         * Arithmetic multiplication of this instance with the value 16. This is
         * internally implemented completely through shifting or at least
         * partially and is therefor faster than the generic mul.
         */
        void mul_16() {
            left_shift(4);
        }

        /**
         * A bitwise left shift which moves all bits cnt positions to the left. Cnt
         * must be <= sizeof(THalf) * 8 as otherwise an exception gets thrown.
         * @param cnt number of bit positions to shift to the left.
         */
        void left_shift(THalf cnt);

        /**
         * A bitwise right shift which moves all bits cnt positions to the right. Cnt
         * must be <= sizeof(THalf) * 8 as otherwise an exception gets thrown.
         * @param cnt number of bit positions to shift to the right.
         */
        THalf right_shift(THalf cnt);

        /**
         * A utility function to convert the value of this instance into a string
         * representation with the given numerical_base. Currently supported are
         * the following numerical_base values 8, 10, 16
         * @param numerical_base base which controls the base of the output string.
         * @return string representation of this instance
         */
        std::string to_string(int numerical_base);

        /**
         * A utility function to reset this BigInt instance to one initialized with a 0.
         */
        void clear();

        /**
         * A utility function to copy the value of another BigInt instance.
         */
        void set(const its::BigInt<TFull, THalf> &source);

        bool operator>=(const its::BigInt<TFull, THalf> &other) const;

        bool operator>=(const long long int &other) const;

        bool operator>(const its::BigInt<TFull, THalf> &other) const;

        bool operator>(const long long int &other) const;

        bool operator<=(const its::BigInt<TFull, THalf> &other) const;

        bool operator<=(const long long int &other) const;

        bool operator<(const its::BigInt<TFull, THalf> &other) const;

        bool operator<(const long long int &other) const;

        bool operator==(const its::BigInt<TFull, THalf> &other) const;

        bool operator==(const long long int &other) const;

        bool operator!=(const its::BigInt<TFull, THalf> &other) const;

        bool operator!=(const long long int &other) const;

        BigInt<TFull, THalf> &operator=(const long long int value) {
            clear();
            positive = value > 0;
            cells = int_to_cell_like(value);
            return *this;
        }

        int comp(its::BigInt<TFull, THalf> &other) {
            if (*this > other) {
                return 1;
            } else if (*this == other) {
                return 0;
            }
            return -1;
        }

#pragma region task 2 extension

        /**
         * Efficiently squares the given num and saves the output in dest.
         * @param num value to square.
         * @param dest result output container.
         */
        void square(BigInt<TFull, THalf> &num, BigInt<TFull, THalf> &dest);

        /**
         * Efficiently squares this instance and saves the output in dest.
         * @param dest result output container.
         */
        void square(BigInt<TFull, THalf> &dest) {
            square(*this, dest);
        }

        /**
         * Efficiently squares this instance and saves it in this instance.
         */
        void square() {
            BigInt<TFull, THalf> num(*this);
            square(num, *this);
        }

        /**
         * Raises num by the power of exp and safes the result in dest.
         * @param num base of the operation.
         * @param exp exponent of the operation.
         * @param dest result of the operation.
         */
        void pow(BigInt<TFull, THalf> &num,
                 BigInt<TFull, THalf> &exp,
                 BigInt<TFull, THalf> &dest);

        /**
         * Raises this instance by the power of exp and safes the result in this instance.
         * @param exp exponent of the operation.
         */
        void pow(BigInt<TFull, THalf> &exp) {
            BigInt<TFull, THalf> num(*this);
            pow(num, exp, *this);
        }

        /**
         * Raises this instance by the power of exp and safes the result in dest.
         * @param exp exponent of the operation.
         * @param dest result of the operation.
         */
        void pow(BigInt<TFull, THalf> &exp, BigInt<TFull, THalf> &dest) {
            BigInt<TFull, THalf> num(*this);
            pow(num, exp, dest);
        }

        /**
         * Executes a pow op followed by a modulo operation like num^{exp} mod module.
         * @param num base of the pow function.
         * @param exp exponent of the pow function.
         * @param module used module of the mod operation.
         * @param dest output of the result.
         */
        void pow_mod(BigInt<TFull, THalf> &num,
                     BigInt<TFull, THalf> &exp,
                     BigInt<TFull, THalf> &module,
                     BigInt<TFull, THalf> &dest);

        /**
         * Executes a pow op followed by a modulo operation like num^{exp} mod module.
         * @param exp exponent of the pow function.
         * @param module used module of the mod operation.
         * @param dest output of the result.
         */
        void mod_pow(BigInt<TFull, THalf> &exp,
                     BigInt<TFull, THalf> &module,
                     BigInt<TFull, THalf> &dest) {

            BigInt<TFull, THalf> num(*this);
            pow_mod(num, exp, module, dest);
        }

        /**
         * Executes a pow op followed by a modulo operation like num^{exp} mod module.
         * @param exp exponent of the pow function.
         * @param module used module of the mod operation.
         */
        void mod_pow(its::BigInt<TFull, THalf> &exp, BigInt<TFull, THalf> &module) {
            BigInt<TFull, THalf> num(*this);
            pow_mod(num, exp, module, *this);
        }

        /**
         * Executes the binary GCD algorithm (stein algorithm) with the given numbers.
         * @param first_num first number which gets tested.
         * @param second_num second number which gets tested.
         * @param dest resulting gcd.
         */
        void gcd_bin(
                its::BigInt<TFull, THalf> &first_num,
                its::BigInt<TFull, THalf> &second_num,
                its::BigInt<TFull, THalf> &dest);

        /**
         * Executes the binary GCD algorithm (stein algorithm) with the given numbers.
         * @param first_num first number which gets tested.
         * @param second_num second number which gets tested.
         */
        void gcd_bin(
                its::BigInt<TFull, THalf> &first_num,
                its::BigInt<TFull, THalf> &second_num) {
            gcd_bin(first_num, second_num, *this);
        }

        /**
         * Utility function which returns true if this instance of a BigInt
         * is odd.
         * @return true if odd.
         */
        bool is_odd();

        /**
         * Utility function to generate a random BigInt<TFull, THalf> number
         * with the given number with bits. The number of bits means in this case
         * the index of the highest set bit in the BigInt<TFull, THalf>.
         * @param num_bits max number of bits the resulting random has.
         * @param enforce_highest_bit if true the number has always num_bits and never fewer.
         * @return A random number with max num_bits or exactly num_bits
         */
        static its::BigInt<TFull, THalf> generate_random(size_t num_bits, bool enforce_highest_bit = false);

        /**
         * Utility function to generate a random BigInt<TFull, THalf> number between
         * 0 and the given upper_limit. The resulting number can contain both bounds!
         * @param upper_limit max possible value
         * @return A random number between 0 and upper_limit including the bounds
         */
        static its::BigInt<TFull, THalf> generate_random(BigInt<TFull, THalf> &upper_limit);


        /**
         * Utility function to generate a random BigInt<TFull, THalf> number between
         * lower_limit and the given upper_limit. The resulting number can contain both bounds!
         * @param lower_limit min possible value
         * @param upper_limit max possible value
         * @return A random number between lower_limit and upper_limit including the bounds
         */
        static its::BigInt<TFull, THalf> generate_random(
                BigInt<TFull, THalf> &lower_limit,
                BigInt<TFull, THalf> &upper_limit);

        /**
         * Utility function to generate a random odd BigInt<TFull, THalf> number
         * with the given number with bits. The number of bits means in this case
         * the index of the highest set bit in the BigInt<TFull, THalf>.
         * @param num_bits max number of bits the resulting random has.
         * @param enforce_highest_bit if true the number has always num_bits and never fewer.
         * @return A random number with max num_bits or exactly num_bits
         */
        static its::BigInt<TFull, THalf> generate_random_odd(size_t num_bits, bool enforce_highest_bit = true);

        /**
         * Executes the fermat prime test on this instance and returns the result.
         * @param base the base with which test should be run.
         * @param gcd_variant if true the gcd based version will be used and otherwise the simple one.
         * @return true if maybe prime and false otherwise.
         */
        bool is_fermat_prime(BigInt<TFull, THalf> &base, bool gcd_variant = true);

        /**
         * Executes the fermat prime test for multiple bases on this instance and returns the result.
         * @param bases a vector of bases which are used for the tests.
         * @param gcd_variant if true the gcd based version will be used and otherwise the simple one.
         * @return false if one of the tests with the given bases fails and otherwise true.
         */
        bool is_fermat_prime(std::vector<its::BigInt<TFull, THalf>> &bases, bool gcd_variant = true);

        /**
         * Executes the euler prime test on this instance and returns the result.
         * @param base the base with which test should be run.
         * @return true if maybe prime and false otherwise.
         */
        bool is_euler_prime(its::BigInt<TFull, THalf> &base);

        /**
         * Executes the euler prime test for multiple bases on this instance and returns the result.
         * @param bases a vector of bases which are used for the tests.
         * @return false if one of the tests with the given bases fails and otherwise true.
         */
        bool is_euler_prime(std::vector<its::BigInt<TFull, THalf>> &bases);

        /**
         * Executes the miller rabin prime test on this instance and returns the result.
         * @param base the base with which test should be run.
         * @return true if maybe prime and false otherwise.
         */
        bool is_mr_prime(its::BigInt<TFull, THalf> &base);

        /**
         * Executes the miller rabin prime test for multiple bases on this instance and returns the result.
         * @param bases a vector of bases which are used for the tests.
         * @return false if one of the tests with the given bases fails and otherwise true.
         */
        bool is_mr_prime(std::vector<its::BigInt<TFull, THalf>> &bases);

        /**
         * Executes a simple division prime test on this instance with the given numbers.
         * @param numbers a vector of number which are used for this test.
         * @return false if one of the tests with the numbers shows a gcd != 1 and otherwise true.
         */
        bool is_div_prime(std::vector<uint64_t> &numbers);

#pragma endregion

#pragma region task 3 extension
        /**
         * Utility function which returns true if this instance of a BigInt
         * is even.
         * @return true if even.
         */
        bool is_even() {
            return !is_odd();
        }

        /**
         * Returns the number of bits needed to represent this number.
         * @return number of bits.
         */
        size_t needed_bits() {
            return cells.needed_bits();
        }

        /**
         * Generates numbers until a prime number with num_bits has been found.
         * @param num_bits number of bits the prime number should have.
         * @param enforce_highest_bit if true the generated numbers are always
         *      num_bits long.
         * @return generated prime number.
         */
        static its::BigInt<TFull, THalf> generate_random_prime(
                size_t num_bits, bool enforce_highest_bit = false);

        /**
         * Runs the extended binary euclidean algorithm. At least one of the
         * numbers must be odd as otherwise a std::runtime exceptions gets thrown.
         * @param first_num first number to get tested.
         * @param second_num second number to get tested.
         * @param gcd_dest resulting gcd.
         * @param u_dest linear combination var for the first_num.
         * @param v_dest linear combination var for the second_num.
         */
        void egcd_bin(
                its::BigInt<TFull, THalf> &first_num,
                its::BigInt<TFull, THalf> &second_num,
                its::BigInt<TFull, THalf> &gcd_dest,
                its::BigInt<TFull, THalf> &u_dest,
                its::BigInt<TFull, THalf> &v_dest);

        /**
         * Runs the extended euclidean algorithm. At least one of the
         * numbers must be odd as otherwise a std::runtime exceptions gets thrown.
         * @param first_num first number to get tested.
         * @param second_num second number to get tested.
         * @param gcd_dest resulting gcd.
         * @param u_dest linear combination var for the first_num.
         * @param v_dest linear combination var for the second_num.
         */
        void egcd(
                its::BigInt<TFull, THalf> &first_num,
                its::BigInt<TFull, THalf> &second_num,
                its::BigInt<TFull, THalf> &gcd_dest,
                its::BigInt<TFull, THalf> &u_dest,
                its::BigInt<TFull, THalf> &v_dest);

        /**
         * Calculates the inverse element through the egcd.
         * @param first_num
         * @param second_num
         * @param dest calculated inverse element.
         */
        void calc_inverse_element(
                its::BigInt<TFull, THalf> &first_num,
                its::BigInt<TFull, THalf> &second_num,
                its::BigInt<TFull, THalf> &dest) {

            BigInt<TFull, THalf> gcd, u, v;
            egcd_bin(first_num, second_num, gcd, u, v);

            if (u < 0) {
                u.add(second_num);
            }

            dest.clear();
            dest.set(u);
        }

        /**
         * Calculates the inverse element through the egcd.
         * @param second_num
         * @param dest calculated inverse element.
         */
        void calc_inverse_element(
                its::BigInt<TFull, THalf> &second_num,
                its::BigInt<TFull, THalf> &dest) {
            calc_inverse_element(*this, second_num, dest);
        }

        /**
         * Calculates num mod module.
         * @param num
         * @param module
         * @param remainder
         * @param round_dest
         */
        void mod(BigInt<TFull, THalf> &num,
                 BigInt<TFull, THalf> &module,
                 BigInt<TFull, THalf> &remainder,
                 bool round_dest = true) {

            BigInt<TFull, THalf> div_result;
            num.divmod(module, div_result, remainder, round_dest);
        }

        /**
         * Calculates "this instance" mod module and writes it back into
         * this instance.
         * @param module
         * @param round_dest
         */
        void mod(BigInt<TFull, THalf> &module,
                 bool round_dest = true) {

            BigInt<TFull, THalf> num{*this};
            BigInt<TFull, THalf> div_result;
            num.divmod(num, module, div_result, *this, round_dest);
        }

#pragma endregion

    };

    // useful aliases for possible types
    using BigInt16 = BigInt<uint16_t, uint8_t>;
    using BigInt32 = BigInt<uint32_t, uint16_t>;
    using BigInt64 = BigInt<uint64_t, uint32_t>;

}

// inclusion of the template implementation sub headers
#include "BigIntArithmeticOps.tpp"
#include "BigIntComparisonOps.tpp"
#include "BigIntUtils.tpp"
#include "BigIntPrimeArithmeticOps.tpp"
#include "BigIntRsaHelperOps.tpp"
