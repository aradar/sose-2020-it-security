#pragma once

#include <cstddef>
#include <vector>

namespace its {
    template<class THalf>
    struct CellParts {
        THalf lower;
        THalf upper;
    };

    template<class TFull, class THalf>
    union Cell {
        TFull raw;
        CellParts<THalf> parts;
    };

    template<class TFull, class THalf>
    class CellStorage {
    private:
        std::vector<Cell<TFull, THalf>> cells;
        size_t index;

    public:
        explicit CellStorage();

        CellStorage(const CellStorage<TFull, THalf> &source);

        CellStorage(std::vector<THalf> cell_likes);

        void update_idx();

        void update_idx(size_t new_idx);

        void increment_idx(size_t increment);

        void insert_zeros(size_t num_zeros);

        size_t curr_idx();

        size_t needed_bits();

        Cell<TFull, THalf> &operator[](size_t pos);

        void clear();

        bool is_empty();

        size_t cnt_trailing_zero_bits();

        bool operator>=(const CellStorage<TFull, THalf> &other) const;

        bool operator>(const CellStorage<TFull, THalf> &other) const;

        bool operator<=(const CellStorage<TFull, THalf> &other) const;

        bool operator<(const CellStorage<TFull, THalf> &other) const;

        bool operator==(const CellStorage<TFull, THalf> &other) const;

        bool operator==(const std::vector<THalf> &other) const;

        bool operator!=(const CellStorage<TFull, THalf> &other) const;
    };

    template<class TFull, class THalf>
    CellStorage<TFull, THalf>::CellStorage()
            : cells(1),
              index(0) {}

    template<class TFull, class THalf>
    CellStorage<TFull, THalf>::CellStorage(
            const CellStorage<TFull, THalf> &source)
            : cells(source.cells),
              index(source.index) {}

    template<class TFull, class THalf>
    CellStorage<TFull, THalf>::CellStorage(
            std::vector<THalf> cell_likes)
            : cells(cell_likes.size()),
              index(0) {

        for (size_t i = 0; i < cell_likes.size(); ++i) {
            auto single_cell = cell_likes[i];

            if (single_cell > 0) {
                cells[i].raw = single_cell;
                index = i;
            }
        }
    }

    template<class TFull, class THalf>
    size_t CellStorage<TFull, THalf>::curr_idx() {
        return index;
    }

    template<class TFull, class THalf>
    Cell<TFull, THalf> &CellStorage<TFull, THalf>::operator[](size_t pos) {
        // catches initial potential empty storage's
        if (cells.size() == 0) {
            cells.resize(1);
        }

        if (pos > (cells.size() - 1)) {
            cells.resize(pos + 1);
        }

        return cells[pos];
    }

    template<class TFull, class THalf>
    void CellStorage<TFull, THalf>::insert_zeros(size_t num_zeros) {
        std::vector<Cell<TFull, THalf>> zeros{num_zeros};
        cells.insert(cells.begin(), std::begin(zeros), std::end(zeros));
        index += num_zeros;
    }

    template<class TFull, class THalf>
    void CellStorage<TFull, THalf>::update_idx() {
        if (cells.size() == 0) {
            index = 0;
            return;
        }

        for (size_t i = cells.size() - 1; i > 0; --i) {
            if (cells[i].raw != 0) {
                index = i;
                return;
            }
        }

        index = 0;
    }

    template<class TFull, class THalf>
    void CellStorage<TFull, THalf>::update_idx(size_t new_idx) {
        index = new_idx;
    }

    template<class TFull, class THalf>
    void CellStorage<TFull, THalf>::increment_idx(size_t increment) {
        index += increment;
    }

    template<class TFull, class THalf>
    void CellStorage<TFull, THalf>::clear() {
        cells.clear();
        cells.resize(1);
        index = 0;
    }

    template<class TFull, class THalf>
    bool CellStorage<TFull, THalf>::is_empty() {
        if (index > 0) {
            return false;
        }

        return cells[0].raw == 0;
    }

    template<class TFull, class THalf>
    bool CellStorage<TFull, THalf>::operator>=(const CellStorage<TFull, THalf> &other) const {
        if (index > other.index) {
            return true;
        }
        if (index < other.index) {
            return false;
        }

        for (size_t i = index + 1; i-- != 0;) {
            if (cells[i].raw != other.cells[i].raw) {
                return cells[i].raw > other.cells[i].raw;
            }
        }

        return true;
    }

    template<class TFull, class THalf>
    bool CellStorage<TFull, THalf>::operator>(const CellStorage<TFull, THalf> &other) const {
        if (index > other.index) {
            return true;
        }
        if (index < other.index) {
            return false;
        }

        for (size_t i = index + 1; i-- != 0;) {
            if (cells[i].raw != other.cells[i].raw) {
                return cells[i].raw > other.cells[i].raw;
            }
        }

        return false;
    }

    template<class TFull, class THalf>
    bool CellStorage<TFull, THalf>::operator<=(const CellStorage<TFull, THalf> &other) const {
        if (index < other.index) {
            return true;
        }
        if (index > other.index) {
            return false;
        }

        for (size_t i = index + 1; i-- != 0;) {
            if (cells[i].raw != other.cells[i].raw) {
                return cells[i].raw < other.cells[i].raw;
            }
        }

        return true;
    }

    template<class TFull, class THalf>
    bool CellStorage<TFull, THalf>::operator<(const CellStorage<TFull, THalf> &other) const {
        if (index < other.index) {
            return true;
        }
        if (index > other.index) {
            return false;
        }

        for (size_t i = index + 1; i-- != 0;) {
            if (cells[i].raw != other.cells[i].raw) {
                return cells[i].raw < other.cells[i].raw;
            }
        }

        return false;
    }

    template<class TFull, class THalf>
    bool CellStorage<TFull, THalf>::operator==(const CellStorage<TFull, THalf> &other) const {
        if (index != other.index) {
            return false;
        }

        for (size_t i = index + 1; i-- != 0;) {
            if (cells[i].raw != other.cells[i].raw) {
                return false;
            }
        }

        return true;
    }

    template<class TFull, class THalf>
    bool CellStorage<TFull, THalf>::operator==(const std::vector<THalf> &other) const {
        size_t other_index = 0;

        for (size_t i = 0; i < other.size(); ++i) {
            if (other[i] > 0) {
                other_index = i;
            }
        }

        if (index != other_index) {
            return false;
        }

        for (size_t i = index + 1; i-- != 0;) {
            if (cells[i].raw != other[i]) {
                return false;
            }
        }

        return true;
    }

    template<class TFull, class THalf>
    bool CellStorage<TFull, THalf>::operator!=(const CellStorage<TFull, THalf> &other) const {
        return !(*this == other);
    }

    template<class TFull, class THalf>
    size_t CellStorage<TFull, THalf>::cnt_trailing_zero_bits() {
        size_t counted_bits = 0;
        const size_t bits_per_cell = sizeof(THalf) * 8;
        const THalf test_mask = 0x1u;

        if (is_empty()) {
            return counted_bits;
        }

        for (size_t i = 0; i <= index; ++i) {
            if (cells[i].raw == 0) {
                counted_bits += bits_per_cell;
            } else {
                for (size_t j = 0; j < bits_per_cell; ++j) {
                    if (cells[i].raw & (test_mask << j)) {
                        return counted_bits;
                    }
                    counted_bits += 1;
                }
            }
        }

        return counted_bits;
    }

    template<class TFull, class THalf>
    size_t CellStorage<TFull, THalf>::needed_bits() {
        auto bits = index * sizeof(THalf) * 8;

        THalf cell_value = cells[index].parts.lower;
        if (cell_value > 0) {
            bits++;
            while (cell_value >>= 1) {
                bits++;
            }
        }

        return bits;
    }
}
