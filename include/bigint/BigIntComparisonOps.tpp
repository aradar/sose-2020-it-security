#pragma once

#include <array>

namespace its {
    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator>=(const BigInt <TFull, THalf> &other) const {
        if (positive && !other.positive) {
            return true;
        } else if (!positive && other.positive) {
            return false;
        } else if(!positive && !other.positive) {
            return cells <= other.cells;
        }

        return cells >= other.cells;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator>=(const long long int &other) const {
        BigInt<TFull, THalf> num(other);
        return *this >= num;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator>(const BigInt <TFull, THalf> &other) const {
        if (positive && !other.positive) {
            return true;
        } else if (!positive && other.positive) {
            return false;
        } else if(!positive && !other.positive) {
            return cells < other.cells;
        }

        return cells > other.cells;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator>(const long long int &other) const {
        BigInt<TFull, THalf> num(other);
        return *this > num;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator<=(const BigInt <TFull, THalf> &other) const {
        if (positive && !other.positive) {
            return false;
        } else if (!positive && other.positive) {
            return true;
        } else if(!positive && !other.positive) {
            return cells >= other.cells;
        }

        return cells <= other.cells;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator<=(const long long int &other) const {
        BigInt<TFull, THalf> num(other);
        return *this <= num;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator<(const BigInt <TFull, THalf> &other) const {
        if (positive && !other.positive) {
            return false;
        } else if (!positive && other.positive) {
            return true;
        } else if(!positive && !other.positive) {
            return cells > other.cells;
        }

        return cells < other.cells;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator<(const long long int &other) const {
        BigInt<TFull, THalf> num(other);
        return *this < num;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator==(const BigInt <TFull, THalf> &other) const {
        if (positive != other.positive) {
            return false;
        }

        return cells == other.cells;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator==(const long long int &other) const {
        BigInt<TFull, THalf> num(other);
        return *this == num;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator!=(const BigInt <TFull, THalf> &other) const {
        return !(*this == other);
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::operator!=(const long long int &other) const {
        BigInt<TFull, THalf> num(other);
        return *this != num;
    }

}
