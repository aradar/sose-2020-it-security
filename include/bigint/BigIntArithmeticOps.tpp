#pragma once

#include "BigInt.hpp"

namespace its {
    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::add_or_sub_cells(
            BigInt <TFull, THalf> &first,
            BigInt <TFull, THalf> &second,
            BigInt <TFull, THalf> &dest,
            bool add_numbers,
            bool dest_positive) {

        auto max_cell_idx = std::max(first.cells.curr_idx(), second.cells.curr_idx());

        // do the calculation for all existing cells
        Cell<TFull, THalf> tmp{0};
        THalf over{0};
        for (size_t i = 0; i <= max_cell_idx; i++) {
            if (add_numbers) {
                tmp.raw = first.cells[i].raw + second.cells[i].raw + over;
            } else {
                tmp.raw = first.cells[i].raw - second.cells[i].raw - over;
                tmp.parts.upper = tmp.parts.upper >> (sizeof(THalf) * 8 - 1);
            }
            dest.cells[i].raw = tmp.parts.lower;
            over = tmp.parts.upper;
        }

        // handle possible overflow in last calculation
        if (over > 0) {
            dest.cells[max_cell_idx + 1].raw += over;
        }

        dest.cells.update_idx();
        dest.positive = dest_positive;

        // removes negative zeros
        if (dest.cells.is_empty()) {
            dest.positive = true;
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::direct_cell_add(size_t index, Cell <TFull, THalf> value) {
        Cell<TFull, THalf> tmp{0};
        auto over = value;
        auto curr_idx = index;
        while (over.raw != 0) {
            tmp.raw = cells[curr_idx].raw + over.raw;
            cells[curr_idx].raw = tmp.parts.lower;
            over.raw = tmp.parts.upper;
            curr_idx++;
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::mul_cells(
            BigInt <TFull, THalf> &multiplier,
            Cell <TFull, THalf> &multiplicand,
            BigInt <TFull, THalf> &dest) {

        Cell<TFull, THalf> tmp{0};
        for (size_t i = 0; i <= multiplier.cells.curr_idx(); ++i) {
            tmp.raw = multiplier.cells[i].raw * multiplicand.raw;
            dest.direct_cell_add(i, tmp);
        }

        dest.cells.update_idx();
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::mul_cells(
            BigInt <TFull, THalf> &multiplier,
            BigInt <TFull, THalf> &multiplicand,
            BigInt <TFull, THalf> &dest,
            bool dest_positive) {

        if (&multiplier == &dest || &multiplicand == &dest) {
            throw std::runtime_error("dest and multiplicand can't be the same object!");
        }

        // make sure that dest is empty
        dest.clear();

        Cell<TFull, THalf> tmp{0};
        for (size_t i = 0; i <= multiplicand.cells.curr_idx(); ++i) {
            for (size_t j = 0; j <= multiplier.cells.curr_idx(); ++j) {
                tmp.raw = multiplier.cells[j].raw * multiplicand.cells[i].raw;
                dest.direct_cell_add(i + j, tmp);
            }
        }

        dest.cells.update_idx();
        dest.positive = dest_positive;
    }

    template<class TFull, class THalf>
    Cell <TFull, THalf> BigInt<TFull, THalf>::estimate_cell_div(
            Cell <TFull, THalf> upper,
            Cell <TFull, THalf> lower,
            Cell <TFull, THalf> divisor) {

        Cell<TFull, THalf> dividend = upper;
        if (dividend.raw < divisor.raw) {
            dividend.parts.upper = upper.parts.lower;
            dividend.parts.lower = lower.parts.lower;
        }

        Cell<TFull, THalf> result;
        result.raw = dividend.raw / divisor.raw;
        return result;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::fix_estimation_slow(
            BigInt <TFull, THalf> &tmp,
            BigInt <TFull, THalf> &divisor,
            BigInt <TFull, THalf> &remainder,
            Cell <TFull, THalf> &estimate) {

        if (tmp.cells != remainder.cells) {
            while (tmp.cells > remainder.cells) {
                estimate.raw -= 1;
                add_or_sub_cells(tmp, divisor, tmp, false, true);
            }

            BigInt<TFull, THalf> diff;
            add_or_sub_cells(remainder, tmp, diff, false, true);
            while (diff.cells > divisor.cells) {
                estimate.raw += 1;
                add_or_sub_cells(tmp, divisor, tmp, true, true);
                add_or_sub_cells(remainder, tmp, diff, false, true);
            }
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::fix_estimation(
            BigInt <TFull, THalf> &tmp,
            BigInt <TFull, THalf> &divisor,
            BigInt <TFull, THalf> &remainder,
            Cell <TFull, THalf> &estimate) {

        if (tmp.cells == remainder.cells) {
            return; // nothing to do as it is already correct
        }

        if (divisor.cells > remainder.cells) {
            estimate.raw = 0;
            tmp.clear();
            return;
        }

        if (tmp.cells > remainder.cells) {
            fix_to_big_estimation(tmp, divisor, remainder, estimate);
        } else {
            BigInt<TFull, THalf> diff;
            //std::cout << tmp.to_string(16) << " " << remainder.to_string(16) << std::endl;
            remainder.sub(tmp, diff);
            if (diff.cells < divisor.cells) {
                return;
            }

            fix_to_small_estimation(tmp, divisor, remainder, estimate);
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::fix_to_big_estimation(
            BigInt <TFull, THalf> &tmp,
            BigInt <TFull, THalf> &divisor,
            BigInt <TFull, THalf> &remainder,
            Cell <TFull, THalf> &estimate) {

        auto work_divisor{divisor};
        TFull step{1};

        while (true) {
            estimate.raw -= step;
            add_or_sub_cells(tmp, work_divisor, tmp, false, true);

            if (tmp <= remainder) {
                break;
            }

            step *= 2;
            work_divisor.left_shift(1);
        }

        if (tmp == remainder || step == 1) {
            return;
        }

        estimate.raw += step;
        add_or_sub_cells(tmp, work_divisor, tmp, true, true);
        BigInt<TFull, THalf> diff;
        while (true) {
            if (step > 1) {
                step /= 2;
                work_divisor.right_shift(1);
            }

            estimate.raw -= step;
            add_or_sub_cells(tmp, work_divisor, tmp, false, true);

            if (tmp.cells <= remainder.cells) {
                diff.clear();
                add_or_sub_cells(remainder, tmp, diff, false, true);
                if (diff.cells < divisor.cells) {
                    return;
                }

                estimate.raw += step;
                add_or_sub_cells(tmp, work_divisor, tmp, true, true);
            }
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::fix_to_small_estimation(
            BigInt <TFull, THalf> &tmp,
            BigInt <TFull, THalf> &divisor,
            BigInt <TFull, THalf> &remainder,
            Cell <TFull, THalf> &estimate) {

        auto work_divisor{divisor};
        TFull step{1};

        while (true) {
            estimate.raw += step;
            add_or_sub_cells(tmp, work_divisor, tmp, true, true);

            if (tmp.cells >= remainder.cells) {
                break;
            }

            step *= 2;
            work_divisor.left_shift(1);
        }

        if (tmp == remainder || step == 1) {
            return;
        }

        estimate.raw -= step;
        add_or_sub_cells(tmp, work_divisor, tmp, false, true);
        BigInt<TFull, THalf> diff;
        while (true) {
            if (step > 1) {
                step /= 2;
                work_divisor.right_shift(1);
            }

            estimate.raw += step;
            add_or_sub_cells(tmp, work_divisor, tmp, true, true);

            if (tmp.cells >= remainder.cells) {
                if (tmp.cells == remainder.cells) {
                    return;
                }

                estimate.raw -= step;
                add_or_sub_cells(tmp, work_divisor, tmp, false, true);

                diff.clear();
                add_or_sub_cells(remainder, tmp, diff, false, true);
                if (diff.cells < divisor.cells) {
                    return;
                }
            }
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::double_cell_moddiv(
            BigInt <TFull, THalf> &dividend,
            BigInt <TFull, THalf> &divisor,
            BigInt <TFull, THalf> &dest,
            BigInt <TFull, THalf> &remainder) {

        auto dividend_cell = dividend.cells[0];
        dividend_cell.parts.upper = dividend.cells[1].parts.lower;
        auto divisor_cell = divisor.cells[0];
        divisor_cell.parts.upper = divisor.cells[1].parts.lower;

        Cell<TFull, THalf> res;
        Cell<TFull, THalf> rem;
        res.raw = dividend_cell.raw / divisor_cell.raw;
        rem.raw = dividend_cell.raw % divisor_cell.raw;

        dest.cells[0].raw = res.parts.lower;
        dest.cells[1].raw = res.parts.upper;
        remainder.cells[0].raw = rem.parts.lower;
        remainder.cells[1].raw = rem.parts.upper;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::cell_storage_moddiv(
            BigInt <TFull, THalf> &dividend,
            BigInt <TFull, THalf> &divisor,
            BigInt <TFull, THalf> &dest,
            BigInt <TFull, THalf> &remainder) {

        size_t len_divisor = divisor.cells.curr_idx() + 1;
        size_t len_diff = dividend.cells.curr_idx() + 1 - len_divisor;

        for (size_t i = 0; i < len_divisor; ++i) {
            remainder.cells[i] = dividend.cells[len_diff + i];
        }
        remainder.cells.update_idx();

        for (size_t des_idx = len_diff + 1; des_idx-- != 0;) {
            auto divisor_idx = divisor.cells.curr_idx();
            auto rem_idx = remainder.cells.curr_idx();
            auto rem_idx_u = rem_idx;
            auto rem_idx_l = rem_idx - 1;

            if (rem_idx == 0) {
                rem_idx_u = 1;
                rem_idx_l = 0;
            }

            auto estimate = estimate_cell_div(
                    remainder.cells[rem_idx_u],
                    remainder.cells[rem_idx_l],
                    divisor.cells[divisor_idx]);

            BigInt<TFull, THalf> tmp;
            mul_cells(divisor, estimate, tmp);

            fix_estimation(tmp, divisor, remainder, estimate);
            //fix_estimation_slow(tmp, divisor, remainder, estimate);

            dest.cells[des_idx] = estimate;
            add_or_sub_cells(remainder, tmp, remainder, false, true);

            // push new cells into remainder as we go
            if (des_idx > 0) {
                remainder.left_shift(sizeof(THalf) * 8);
                remainder.direct_cell_add(0, dividend.cells[des_idx - 1]);
                remainder.cells.update_idx();
            }
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::moddiv_cells(
            BigInt <TFull, THalf> &dividend,
            BigInt <TFull, THalf> &divisor,
            BigInt <TFull, THalf> &dest,
            BigInt <TFull, THalf> &remainder,
            bool dest_positive) {

        dest.clear();
        remainder.clear();
        // init remainder with dividend in case of an early return
        remainder.set(dividend);

        if (divisor.cells.is_empty()) {
            throw std::runtime_error("divisor is 0 and division by zero is not allowed!");
        }

        if (dividend.cells == divisor.cells) {
            dest.cells[0].raw = 1;
            remainder.clear();
            dest.positive = dest_positive;
            return;
        }

        if (dividend.cells < divisor.cells) {
            if (!dividend.positive) {
                auto abs_divisor{divisor};
                abs_divisor.positive = true;
                remainder.add(abs_divisor);
            }
            return;
        }

        // reinitialize remainder for real div
        remainder.clear();

        // if the numbers are each only two or less cells wide merge and
        // compute directly
        if (dividend.cells.curr_idx() < 2 && divisor.cells.curr_idx() < 2) {
            double_cell_moddiv(dividend, divisor, dest, remainder);
        } else {
            cell_storage_moddiv(dividend, divisor, dest, remainder);
        }

        remainder.cells.update_idx();

        // handling of negative remainder by getting the inverse value of the
        // remainder inside the divisor int modulo ring
        if (!remainder.cells.is_empty() && !dividend.positive) {
            add_or_sub_cells(
                    divisor,
                    remainder,
                    remainder,
                    false,
                    true);
        }

        dest.cells.update_idx();
        dest.positive = dest_positive;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::left_shift(THalf cnt) {
        if (cnt == 0) {
            return;
        }

        const auto bytes_per_half_cell = sizeof(THalf) * 8;

        auto full_shifts = cnt / bytes_per_half_cell;
        auto partial_shifts = cnt % bytes_per_half_cell;

        if (full_shifts > 0) {
            cells.insert_zeros(full_shifts);
        }

        if (partial_shifts > 0) {
            THalf over{0};
            Cell<TFull, THalf> tmp{0};
            for (size_t i = 0; i <= cells.curr_idx(); i++) {
                tmp.raw = (cells[i].raw << partial_shifts) + over;
                cells[i].raw = tmp.parts.lower;
                over = tmp.parts.upper;
            }

            if (over > 0) {
                cells.increment_idx(1);
                cells[cells.curr_idx()].raw = over;
            }
            cells.update_idx();
        }

    }

    template<class TFull, class THalf>
    THalf BigInt<TFull, THalf>::right_shift(THalf cnt) {
        if (cnt == 0) {
            return 0u;
        }

        if (cnt > sizeof(THalf) * 8) {
            throw std::runtime_error("given cnt is bigger than the number of bits from a single cell!");
        }

        THalf under{0};
        for (size_t i = cells.curr_idx() + 1; i-- != 0;) {
            // safe cell value in upper part to catch values which fall off
            Cell<TFull, THalf> tmp{0};
            tmp.parts.upper = cells[i].parts.lower;
            tmp.raw = tmp.raw >> cnt;
            // put current upper part and previous fallen off values into the
            // current cell
            cells[i].raw = tmp.parts.upper + under;
            under = tmp.parts.lower;
        }

        cells.update_idx();
        // prevents negative zeros
        if (cells.is_empty()) {
            positive = true;
        }

        return under;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::add(
            BigInt <TFull, THalf> &first,
            BigInt <TFull, THalf> &second,
            BigInt <TFull, THalf> &dest) {

        if (first.positive && second.positive) {
            add_or_sub_cells(first, second, dest, true, true);
        } else if (!first.positive && !second.positive) {
            add_or_sub_cells(first, second, dest, true, false);
        } else if (first.positive && !second.positive) {
            if (first.cells > second.cells) {
                add_or_sub_cells(first, second, dest, false, true);
            } else {
                add_or_sub_cells(second, first, dest, false, false);
            }
        } else if (!first.positive && second.positive) {
            if (first.cells > second.cells) {
                add_or_sub_cells(first, second, dest, false, false);
            } else {
                add_or_sub_cells(second, first, dest, false, true);
            }
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::sub(
            BigInt <TFull, THalf> &first,
            BigInt <TFull, THalf> &second,
            BigInt <TFull, THalf> &dest) {

        if (first.positive && second.positive) {
            if (first.cells > second.cells) {
                add_or_sub_cells(first, second, dest, false, true);
            } else {
                add_or_sub_cells(second, first, dest, false, false);
            }
        } else if (!first.positive && !second.positive) {
            if (first.cells > second.cells) {
                add_or_sub_cells(first, second, dest, false, false);
            } else {
                add_or_sub_cells(second, first, dest, false, true);
            }
        } else if (first.positive && !second.positive) {
            add_or_sub_cells(first, second, dest, true, true);
        } else if (!first.positive && second.positive) {
            add_or_sub_cells(first, second, dest, true, false);
        }
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::mul(
            BigInt <TFull, THalf> &multiplier,
            BigInt <TFull, THalf> &multiplicand,
            BigInt <TFull, THalf> &dest) {

        bool res_pos = true;

        if (multiplier.positive && !multiplicand.positive || !multiplier.positive && multiplicand.positive) {
            res_pos = false;
        }

        mul_cells(multiplier, multiplicand, dest, res_pos);
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::divmod(
            BigInt <TFull, THalf> &dividend,
            BigInt <TFull, THalf> &divisor,
            BigInt <TFull, THalf> &dest,
            BigInt <TFull, THalf> &remainder,
            bool round_dest) {

        bool res_pos = true;

        if (dividend.positive && !divisor.positive || !dividend.positive && divisor.positive) {
            res_pos = false;
        }

        moddiv_cells(dividend, divisor, dest, remainder, res_pos);

        if (round_dest) {
            if ((!dividend.positive || !divisor.positive) && !remainder.cells.is_empty()) {
                BigInt<TFull, THalf> one(1);
                if (!dividend.positive) {
                    add_or_sub_cells(dest, one, dest, true, res_pos);
                }
            }
        }
    }

}
