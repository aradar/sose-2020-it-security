#pragma once

#include <iomanip>

namespace its {
    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::from_string(const std::string &value_string) {
        if (value_string.empty()) {
            return;
        }

        // extract leading sign
        auto work_string = value_string;
        auto first_char = work_string.at(0);
        if (first_char == '+' || first_char == '-') {
            work_string.erase(0, 1);
        }

        // extract numerical base information
        auto num_base = 10;
        if (work_string.find("0x") == 0 || work_string.find("0X") == 0) {
            num_base = 16;
            work_string.erase(0, 2);
        } else if (work_string.find("0o") == 0 || work_string.find("0O") == 0) {
            num_base = 8;
            work_string.erase(0, 2);
        }

        // remove leading zeros
        work_string.erase(
                0,
                std::min(
                        work_string.find_first_not_of('0'),
                        work_string.size() - 1));

        for (auto i = 0; i < work_string.size(); i++) {
            auto char_str = work_string.substr(i, 1);
            auto val = std::strtoul(char_str.c_str(), nullptr, num_base);

            if (num_base == 8) {
                mul_8();
            } else if (num_base == 10) {
                mul_10();
            } else if (num_base == 16) {
                mul_16();
            }

            auto cell_val = Cell<TFull, THalf>{0};
            cell_val.parts.lower = val;

            direct_cell_add((THalf) 0, cell_val);
        }

        if (first_char == '+' || first_char == '-') {
            positive = first_char == '+';
        }

        cells.update_idx();
    }

    template<class TFull, class THalf>
    std::string BigInt<TFull, THalf>::to_string(int numerical_base) {
        auto prefix = std::string("");
        auto format_func = std::dec;

        if (numerical_base == 8) {
            prefix = std::string("0o");
            format_func = std::oct;
        } else if (numerical_base == 16) {
            prefix = std::string("0x");
            format_func = std::hex;
        } else if (numerical_base != 10) {
            throw std::runtime_error("numerical_base must be either 8, 10 or 16!");
        }

        auto copy{*this};
        // force the copy to be positive to remove for this case incorrect
        // negative remainder rounding
        copy.positive = true;

        BigInt<TFull, THalf> remainder;
        BigInt<TFull, THalf> result;
        BigInt<TFull, THalf> divisor;
        divisor.cells[0].raw = numerical_base;

        std::stringstream stream;
        if (copy.cells.is_empty()) {
            stream << format_func << remainder.cells[0].raw;
        } else {
            while (!copy.cells.is_empty()) {
                copy.moddiv_cells(copy, divisor, result, remainder, true);
                copy.set(result);
                stream << format_func << remainder.cells[0].raw;
            }
        }

        auto num_str = std::string(stream.str());
        std::reverse(num_str.begin(), num_str.end());

        num_str.insert(0, prefix);
        num_str.insert(0, 1, positive ? '+' : '-');

        return num_str;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::clear() {
        positive = true;
        cells.clear();
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::set(const BigInt <TFull, THalf> &source) {
        positive = source.positive;
        cells = {source.cells};
    }

    template<class TFull, class THalf>
    std::vector<THalf> BigInt<TFull, THalf>::int_to_cell_like(long long int value) {
        long long int abs_value = std::abs(value);
        auto *value_ptr = reinterpret_cast<THalf*>(&abs_value);
        size_t num_cell_likes = sizeof(abs_value) / sizeof(THalf);
        std::vector<THalf> cell_likes;

        size_t last_non_zero = 0;

        for (size_t i = 0; i < num_cell_likes; ++i) {
            THalf temp_value = *(value_ptr + i);
            cell_likes.push_back(temp_value);

            if (temp_value != 0) {
                last_non_zero = i;
            }
        }

        if (last_non_zero > 0) {
            cell_likes.erase(cell_likes.begin() + last_non_zero + 1, cell_likes.end());
        } else {
            cell_likes.resize(1);
        }

        return cell_likes;
    }

}
