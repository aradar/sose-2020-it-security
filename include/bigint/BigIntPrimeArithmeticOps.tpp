#pragma once

// this file contains the implementation for the methods of task 2 inside the
// BigInt<TF, TH> class.

namespace its {

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::is_odd() {
        return cells[0].raw & 0x1;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::square(BigInt <TFull, THalf> &num, BigInt <TFull, THalf> &dest) {
        if (&num == &dest) {
            throw std::runtime_error("num and dest can't be the same object!");
        }

        Cell<TFull, THalf> tmp;
        dest.clear();

        for (size_t i = 0; i <= num.cells.curr_idx(); ++i) {
            for (size_t j = i + 1; j <= num.cells.curr_idx(); ++j) {
                tmp.raw = num.cells[i].raw * num.cells[j].raw;
                dest.direct_cell_add(i + j, tmp);
                dest.direct_cell_add(i + j, tmp);
            }
            tmp.raw = num.cells[i].raw * num.cells[i].raw;
            dest.direct_cell_add(2 * i, tmp);
        }

        dest.cells.update_idx();
        dest.positive = true;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::pow(
            BigInt <TFull, THalf> &num,
            BigInt <TFull, THalf> &exp,
            BigInt <TFull, THalf> &dest) {

        if (&num == &exp || &num == &dest || &exp == &dest) {
            throw std::runtime_error("num, exp and dest must all be different instances!");
        }

        if (!exp.positive) {
            throw std::runtime_error("negative exp values are not supported!");
        }

        dest.clear();
        dest.cells[0].raw = 1;
        dest.cells.update_idx();
        BigInt<TFull, THalf> temp(num);

        while (!exp.cells.is_empty()) {
            auto under = exp.right_shift(1);
            if (under) {
                dest.mul(temp);
            }
            temp.square();
        }

        // todo: fix positive flag
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::pow_mod(
            BigInt <TFull, THalf> &num,
            BigInt <TFull, THalf> &exp,
            BigInt <TFull, THalf> &module,
            BigInt <TFull, THalf> &dest) {

        if (!exp.positive) {
            throw std::runtime_error("negative exp values are not supported!");
        }

        if (!module.positive) {
            throw std::runtime_error("negative module values are not supported!");
        }

        dest.clear();
        dest.cells[0].raw = 1;
        dest.cells.update_idx();
        BigInt<TFull, THalf> exp_copy(exp);
        BigInt<TFull, THalf> num_copy(num);
        BigInt<TFull, THalf> mod_temp(num);

        while (!exp_copy.cells.is_empty()) {
            if (exp_copy.is_odd()) {
                dest.mul(num_copy, mod_temp);
                mod_temp.divmod(module, dest);
            }
            num_copy.square(mod_temp);
            mod_temp.divmod(module, num_copy);
            exp_copy.right_shift(1);
        }

        // todo: fix positive flag
    }

    template<class TFull, class THalf>
    SimplePrimeResult BigInt<TFull, THalf>::basic_prime_check() {
        if (*this <= 1) {
            return SimplePrimeResult::no;
        }

        if (*this == 2) {
            return SimplePrimeResult::yes;
        }

        if (is_odd()) {
            return SimplePrimeResult::maybe;
        }

        return SimplePrimeResult::no;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::is_fermat_prime(BigInt <TFull, THalf> &base, bool gcd_variant) {
        if (!gcd_variant) {
            BigInt<TFull, THalf> result;
            pow_mod(base, *this, *this, result);
            return result == base;
        }

        BigInt<TFull, THalf> one(1);

        BigInt<TFull, THalf> gcd;
        gcd.gcd_bin(base, *this);
        if (*this != base && gcd != one) {
            return false;
        }

        BigInt<TFull, THalf> candidate(*this);
        candidate.sub(one);

        BigInt<TFull, THalf> result;
        pow_mod(base, candidate, *this, result);
        return result == one;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::is_euler_prime(BigInt <TFull, THalf> &base) {
        BigInt<TFull, THalf> one(1);

        BigInt<TFull, THalf> gcd;
        gcd.gcd_bin(base, *this);
        if (*this != base && gcd != one) {
            return false;
        }

        BigInt<TFull, THalf> candidate(*this);
        candidate.sub(one);
        candidate.right_shift(1);

        BigInt<TFull, THalf> result;
        pow_mod(base, candidate, *this, result);

        BigInt<TFull, THalf> candidate_minus_one(*this);
        candidate_minus_one.sub(one);
        return result == 1 || result == -1 || result == candidate_minus_one;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::is_mr_prime(BigInt <TFull, THalf> &base) {
        BigInt<TFull, THalf> one(1);
        BigInt<TFull, THalf> minus_one(-1);

        BigInt<TFull, THalf> gcd;
        gcd.gcd_bin(base, *this);
        if (*this != base && gcd != one) {
            return false;
        }

        if (*this == 1) {
            return false;
        }

        BigInt<TFull, THalf> prime_minus_one(*this);
        prime_minus_one.sub(one);

        BigInt<TFull, THalf> exponent(prime_minus_one);
        auto s = 0;
        while (exponent.is_even()) {
            exponent.right_shift(1);
            s++;
        }

        BigInt<TFull, THalf> result;
        pow_mod(base, exponent, *this, result);
        if (result == one || result == prime_minus_one || result == minus_one) {
            return true;
        }

        BigInt<TFull, THalf> div_result;
        for (int i = 0; i < s; ++i) {
            result.square();
            result.mod(*this);

            if (result == prime_minus_one || result == minus_one) {
                return true;
            } else if (result == one) {
                return false;
            }
        }
        return false;
    }

    template<class TFull, class THalf>
    void BigInt<TFull, THalf>::gcd_bin(
            BigInt <TFull, THalf> &first_num,
            BigInt <TFull, THalf> &second_num,
            BigInt <TFull, THalf> &dest) {

        dest.clear();

        if (first_num.cells.is_empty() && second_num.cells.is_empty()) {
            return;
        }

        auto a(first_num);
        auto b(second_num);

        // force both copies to be positive
        a.positive = true;
        b.positive = true;

        auto k = 0u;
        while (a.is_even() && b.is_even()) {
            a.right_shift(1);
            b.right_shift(1);
            ++k;
        }

        while (!a.cells.is_empty()) {
            while (a.is_even()) {
                a.right_shift(1);
            }
            while (b.is_even()) {
                b.right_shift(1);
            }

            if (a < b) {  // todo: is this really supposed to be the signed < op??
                auto temp = a;
                a = b;
                b = temp;
            }
            a.sub(b);
        }

        dest.set(b);
        dest.left_shift(k);
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::is_fermat_prime(
            std::vector<its::BigInt<TFull, THalf>> &bases,
            bool gcd_variant) {

        auto basic_result = basic_prime_check();
        if (basic_result == SimplePrimeResult::yes) {
            return true;
        } else if (basic_result == SimplePrimeResult::no) {
            return false;
        }

        for (auto &curr_base : bases) {
            if (!is_fermat_prime(curr_base, gcd_variant)) {
                return false;
            }
        }
        return true;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::is_euler_prime(std::vector<its::BigInt<TFull, THalf>> &bases) {
        auto basic_result = basic_prime_check();
        if (basic_result == SimplePrimeResult::yes) {
            return true;
        } else if (basic_result == SimplePrimeResult::no) {
            return false;
        }

        for (auto &curr_base: bases) {
            if (!is_euler_prime(curr_base)) {
                return false;
            }
        }
        return true;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::is_mr_prime(std::vector<its::BigInt<TFull, THalf>> &bases) {
        auto basic_result = basic_prime_check();
        if (basic_result == SimplePrimeResult::yes) {
            return true;
        } else if (basic_result == SimplePrimeResult::no) {
            return false;
        }

        for (auto &curr_base: bases) {
            if (!is_mr_prime(curr_base)) {
                return false;
            }
        }
        return true;
    }

    template<class TFull, class THalf>
    bool BigInt<TFull, THalf>::is_div_prime(std::vector<uint64_t> &numbers) {
        for (auto raw_num: numbers) {
            BigInt<TFull, THalf> gcd, num(raw_num);
            gcd_bin(*this, num, gcd);
            if (gcd != 1) {
                return false;
            }
        }
        return true;
    }

    template<class TFull, class THalf>
    BigInt <TFull, THalf> BigInt<TFull, THalf>::generate_random(BigInt <TFull, THalf> &upper_limit) {
        // this generates a number between 0 and upper_limit including both limits

        if (upper_limit == 0) {
            return 0;
        }

        BigInt<TFull, THalf> one = 1;
        const auto bits_per_cell = sizeof(THalf) * 8;
        auto needed_bits = upper_limit.cells.needed_bits();

        if (needed_bits == 1) {
            return generate_random(1);
        }

        // generate the max possible number with 1 bit less than the upper_limit
        BigInt<TFull, THalf> remainder = 1;
        size_t needed_cells = (needed_bits - 1) / bits_per_cell;
        remainder.cells.insert_zeros(needed_cells);
        auto remaining_bits = (needed_bits - 1) - (needed_cells * bits_per_cell);
        if (remaining_bits > 0) {
            remainder.cells[needed_cells].parts.lower <<= (remaining_bits);
        }
        remainder.sub(one);

        // get the difference between the two
        BigInt<TFull, THalf> diff;
        upper_limit.sub(remainder, diff);

        // generate the safe parts of the number
        auto rnd_number = generate_random(needed_bits - 1);
        // rerun this function with the diff to get exactly up to the upper_limit
        auto diff_rnd_number = generate_random(diff);

        // sum up the parts
        rnd_number.add(diff_rnd_number);

        return rnd_number;
    }

    template<class TFull, class THalf>
    BigInt <TFull, THalf> BigInt<TFull, THalf>::generate_random(
            BigInt <TFull, THalf> &lower_limit,
            BigInt <TFull, THalf> &upper_limit) {

        BigInt<TFull, THalf> temp_limit;
        upper_limit.sub(lower_limit, temp_limit);
        BigInt<TFull, THalf> rnd_num = BigInt<TFull, THalf>::generate_random(temp_limit);
        rnd_num.add(lower_limit);
        return rnd_num;
    }

    template<class TFull, class THalf>
    BigInt <TFull, THalf> BigInt<TFull, THalf>::generate_random(
            size_t num_bits,
            bool enforce_highest_bit) {

        if (num_bits == 0) {
            throw std::runtime_error("num_bits must be > 0!");
        }

        const auto bits_per_cell = sizeof(THalf) * 8;

        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<THalf> distrib;

        BigInt<TFull, THalf> num;
        size_t needed_cells = std::ceil(num_bits / static_cast<double>(bits_per_cell));
        for (size_t i = 0; i < needed_cells; ++i) {
            num.cells[i].parts.lower = distrib(gen);
        }
        THalf highest_cell = num.cells[needed_cells - 1].parts.lower;
        // check for not requested bits and remove them if needed
        size_t partial_cell_bits = num_bits % bits_per_cell;
        if (partial_cell_bits > 0) {
            auto bits_to_remove = bits_per_cell - partial_cell_bits;
            highest_cell >>= bits_to_remove;
            if (enforce_highest_bit) {
                highest_cell |= 0x1u << (partial_cell_bits - 1);
            }
        } else if (enforce_highest_bit) {
            highest_cell |= 0x1u << (bits_per_cell - 1);
        }

        num.cells[needed_cells - 1].parts.lower = highest_cell;
        num.cells.update_idx();

        return num;
    }

    template<class TFull, class THalf>
    BigInt <TFull, THalf> BigInt<TFull, THalf>::generate_random_odd(
            size_t num_bits,
            bool enforce_highest_bit) {

        BigInt<TFull, THalf> num = BigInt<TFull, THalf>::generate_random(num_bits, enforce_highest_bit);
        // enforce that the number is odd
        num.cells[0].parts.lower |= 0x1u;

        return num;
    }

}
