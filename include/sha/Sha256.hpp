#pragma once

#include <array>
#include <vector>
#include <cstdint>
#include <db_cxx.h>
#include <iomanip>
#include <sstream>
#include <bitset>

namespace its {

    class Sha256 {

    private:
        const static uint32_t k[64];

        uint32_t hash_values[8];

        std::vector<uint8_t> block;

        size_t next_block_idx;

        uint64_t hashed_bits;

        bool finished;

        static uint32_t rotate_right(uint32_t value, size_t count);

        static uint32_t ch(uint32_t x, uint32_t y, uint32_t z);

        static uint32_t maj(uint32_t x, uint32_t y, uint32_t z);

        static uint32_t big_sig_0(uint32_t x);

        static uint32_t big_sig_1(uint32_t x);

        static uint32_t small_sig_0(uint32_t x);

        static uint32_t small_sig_1(uint32_t x);

        void compute_hash();

    public:

        Sha256()
                : hash_values{0x6a09e667, 0xbb67ae85,
                              0x3c6ef372, 0xa54ff53a,
                              0x510e527f, 0x9b05688c,
                              0x1f83d9ab, 0x5be0cd19},
                  hashed_bits(0),
                  finished(false),
                  block(64),
                  next_block_idx(0) {}

        void update(const char *data, size_t len);

        void hexupdate(std::string &hex_string);

        void finalize();

        std::array<uint32_t, 8> digest();

        std::string hexdigest();

    };

    const uint32_t Sha256::k[] = {
            0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5, 0x3956c25b,
            0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01,
            0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7,
            0xc19bf174, 0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc,
            0x2de92c6f, 0x4a7484aa, 0x5cb0a9dc, 0x76f988da, 0x983e5152,
            0xa831c66d, 0xb00327c8, 0xbf597fc7, 0xc6e00bf3, 0xd5a79147,
            0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138, 0x4d2c6dfc,
            0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
            0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819,
            0xd6990624, 0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08,
            0x2748774c, 0x34b0bcb5, 0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f,
            0x682e6ff3, 0x748f82ee, 0x78a5636f, 0x84c87814, 0x8cc70208,
            0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
    };

    uint32_t Sha256::rotate_right(uint32_t value, size_t count) {
        return (value >> count) | (value << (sizeof(value) * 8 - count));
    }

    uint32_t Sha256::ch(uint32_t x, uint32_t y, uint32_t z) {
        return (x & y) ^ (~x & z);
    }

    uint32_t Sha256::maj(uint32_t x, uint32_t y, uint32_t z) {
        return (x & y) ^ (x & z) ^ (y & z);
    }

    uint32_t Sha256::big_sig_0(uint32_t x) {
        return rotate_right(x, 2) ^ rotate_right(x, 13) ^ rotate_right(x, 22);
    }

    uint32_t Sha256::big_sig_1(uint32_t x) {
        return rotate_right(x, 6) ^ rotate_right(x, 11) ^ rotate_right(x, 25);
    }

    uint32_t Sha256::small_sig_0(uint32_t x) {
        return rotate_right(x, 7) ^ rotate_right(x, 18) ^ (x >> 3u);
    }

    uint32_t Sha256::small_sig_1(uint32_t x) {
        return rotate_right(x, 17) ^ rotate_right(x, 19) ^ (x >> 10u);
    }

    std::array<uint32_t, 8> Sha256::digest() {
        if (!finished) {
            finalize();
        }

        std::array<uint32_t, 8> hash_copy{};

        for (size_t i = 0; i < 8; ++i) {
            hash_copy[i] = hash_values[i];
        }

        return hash_copy;
    }

    std::string Sha256::hexdigest() {
        if (!finished) {
            finalize();
        }

        std::stringstream stream;
        for (unsigned int hash_value : hash_values) {
            stream << std::setfill('0') << std::setw(8) << std::right << std::hex << hash_value;
        }

        return stream.str();
    }

    void Sha256::hexupdate(std::string &hex_string) {
        // remove sign char if it is there
        auto work_string = hex_string;
        auto first_char = work_string.at(0);
        if (first_char == '+' || first_char == '-') {
            work_string.erase(0, 1);
        }

        // remove hex str marker if it is there
        auto hex_char = work_string.at(1);
        if (hex_char == 'x' || hex_char == 'X') {
            work_string.erase(0, 2);
        }

        // if num hex chars is uneven add a 0 to the start
        if (work_string.size() % 2 != 0) {
            work_string.insert(0, "0");
        }

        uint8_t array[work_string.size() / 2];
        for (size_t i = 0; i < work_string.size() / 2; ++i) {
            auto char_str = work_string.substr(i * 2, 2);
            auto val = (uint8_t)std::stoul(char_str, nullptr, 16);
            array[i] = val;
        }

        update(reinterpret_cast<const char *>(array), sizeof(array));
    }

    void Sha256::update(const char *data, size_t len) {
        if (finished) {
            throw std::runtime_error("A finalized Sha256 object can't be updated anymore!");
        }

        for (size_t i = 0; i < len; ++i) {
            block[next_block_idx] = data[i];
            next_block_idx++;

            if (next_block_idx == block.size()) {
                compute_hash();
                hashed_bits += next_block_idx * 8;  // for sha256 this equals 512
                std::fill(block.begin(), block.end(), 0u);
                next_block_idx = 0;
            } else if (next_block_idx > block.size()) {
                throw std::runtime_error("The block has been over committed! This is an internal error!");
            }
        }
    }

    void Sha256::finalize() {
        if (finished) {
            return;
        }

        // update hashed bits a last time to get the partial block
        hashed_bits += next_block_idx * 8;

        // set first bit after last data byte to 1
        block[next_block_idx] = 0x80;
        next_block_idx++;

        if (next_block_idx > 56) {
            compute_hash();
            std::fill(block.begin(), block.end(), 0u);
        }

        block[block.size() - 8] = (hashed_bits >> 56u) & 0xFFu;
        block[block.size() - 7] = (hashed_bits >> 48u) & 0xFFu;
        block[block.size() - 6] = (hashed_bits >> 40u) & 0xFFu;
        block[block.size() - 5] = (hashed_bits >> 32u) & 0xFFu;
        block[block.size() - 4] = (hashed_bits >> 24u) & 0xFFu;
        block[block.size() - 3] = (hashed_bits >> 16u) & 0xFFu;
        block[block.size() - 2] = (hashed_bits >> 8u) & 0xFFu;
        block[block.size() - 1] = hashed_bits & 0xFFu;

        // debug prints of the interanl state during hashing
        //for (int i = 0; i < block.size(); ++i) {
        //    std::cout << std::bitset<8>(block[i]) << " ";
        //}
        //std::cout << std::endl;

        compute_hash();

        std::fill(block.begin(), block.end(), 0u);
        next_block_idx = 0;
        finished = true;
    }

    void Sha256::compute_hash() {
        std::vector<uint32_t> w(64);

        // message schedule part
        for (size_t i = 0; i < 16; ++i) {
            w[i] = block[i * 4] << 24u
                   | block[i * 4 + 1] << 16u
                   | block[i * 4 + 2] << 8u
                   | block[i * 4 + 3];
        }

        for (size_t i = 16; i < 64; ++i) {
            w[i] = small_sig_1(w[i - 2]) + w[i - 7] + small_sig_0(w[i - 15]) + w[i - 16];
        }

        // compression part
        uint32_t a, b, c, d, e, f, g, h, t_0, t_1;

        a = hash_values[0];
        b = hash_values[1];
        c = hash_values[2];
        d = hash_values[3];
        e = hash_values[4];
        f = hash_values[5];
        g = hash_values[6];
        h = hash_values[7];

        for (size_t i = 0; i < 64; ++i) {
            t_0 = h + big_sig_1(e) + ch(e, f, g) + k[i] + w[i];
            t_1 = big_sig_0(a) + maj(a, b, c);
            h = g;
            g = f;
            f = e;
            e = d + t_0;
            d = c;
            c = b;
            b = a;
            a = t_0 + t_1;

            // debug prints of the interanl state during hashing
            //std::cout << std::dec << std::setfill('0') << std::setw(2) << i << " ";
            //std::cout << std::setfill('0') << std::setw(8) << std::hex
            //    << a << " "
            //    << std::setfill('0') << std::setw(8) << std::hex
            //    << b << " "
            //    << std::setfill('0') << std::setw(8) << std::hex
            //    << c << " "
            //    << std::setfill('0') << std::setw(8) << std::hex
            //    << d << " "
            //    << std::setfill('0') << std::setw(8) << std::hex
            //    << e << " "
            //    << std::setfill('0') << std::setw(8) << std::hex
            //    << f << " "
            //    << std::setfill('0') << std::setw(8) << std::hex
            //    << g << " "
            //    << std::setfill('0') << std::setw(8) << std::hex
            //    << h << std::endl;
        }

        hash_values[0] += a;
        hash_values[1] += b;
        hash_values[2] += c;
        hash_values[3] += d;
        hash_values[4] += e;
        hash_values[5] += f;
        hash_values[6] += g;
        hash_values[7] += h;
    }
}
