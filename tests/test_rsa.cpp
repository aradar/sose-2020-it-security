#include <fstream>

#include <catch2/catch.hpp>
#include <nlohmann/json.hpp>

#include <bigint/BigInt.hpp>
#include <rsa/RsaKeys.hpp>
#include <rsa/Rsa.hpp>

using json = nlohmann::json;

template<class T>
void egcd_tests() {
    std::ifstream input_stream("./egcd-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        SECTION(name) {
            T first(element["first_num"].get<std::string>());
            T second(element["second_num"].get<std::string>());

            SECTION("egcd_bin") {
                T gcd_result, u_result, v_result;

                if (first.is_even() && second.is_even()){
                    CHECK_THROWS(gcd_result.egcd_bin(
                            first, second, gcd_result, u_result, v_result));
                } else {
                    gcd_result.egcd_bin(
                            first, second, gcd_result, u_result, v_result);

                    T ua, vb, calculated_gcd;
                    u_result.mul(first, ua);
                    v_result.mul(second, vb);
                    ua.add(vb, calculated_gcd);

                    CHECK_THAT(
                            gcd_result.to_string(16),
                            Catch::Equals(element["gcd_num"].get<std::string>()));
                    CHECK_THAT(
                            calculated_gcd.to_string(16),
                            Catch::Equals(element["gcd_num"].get<std::string>()));
                }
            }

            SECTION("egcd") {
                T gcd_result, u_result, v_result;
                gcd_result.egcd(first, second, gcd_result, u_result, v_result);

                CHECK_THAT(
                        gcd_result.to_string(16),
                        Catch::Equals(element["gcd_num"].get<std::string>()));
                CHECK_THAT(
                        u_result.to_string(16),
                        Catch::Equals(element["u_num"].get<std::string>()));
                CHECK_THAT(
                        v_result.to_string(16),
                        Catch::Equals(element["v_num"].get<std::string>()));
            }
        }
    }
}

void keygen_tests() {
    std::ifstream input_stream("./RSA-Tests-1.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        SECTION(name) {
            its::BigInt64 p(element["p"].get<std::string>());
            its::BigInt64 q(element["q"].get<std::string>());
            its::BigInt64 e(element["e"].get<std::string>());

            auto pair = its::RsaKeyPair::generate(p, q , e);

            SECTION("calculated phi is correct") {
                CHECK_THAT(
                        pair.private_key.phi.to_string(16),
                        Catch::Equals(element["to_find_phi"].get<std::string>()));
            }

            SECTION("calculated d is correct") {
                CHECK_THAT(
                        pair.private_key.d.to_string(16),
                        Catch::Equals(element["to_find_d"].get<std::string>()));
            }
        }
    }
}

void encryption_test(std::string json_path) {
    std::ifstream input_stream(json_path);
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        SECTION(name) {
            its::BigInt64 p(element["p"].get<std::string>());
            its::BigInt64 q(element["q"].get<std::string>());
            its::BigInt64 e(element["e"].get<std::string>());

            its::BigInt64 one = 1, n_minus_one;
            auto keys = its::RsaKeyPair::generate(p, q , e);
            auto rsa = its::Rsa(keys);
            keys.private_key.n.sub(one, n_minus_one);

            for (auto &message_el : element["messages"]) {
                auto raw_str = message_el["raw"].get<std::string>();
                auto encrypted_str = message_el["encrypted"].get<std::string>();
                its::BigInt64 message_num(raw_str);

                if (message_num == 0 || message_num == 1 || message_num == n_minus_one) {
                    SECTION("encryption of " + raw_str + " caused an exception") {
                        CHECK_THROWS(rsa.encrypt(message_num));
                    }
                } else {
                    SECTION("encrypted " + raw_str + " to " + encrypted_str) {
                        auto encrypted_num = rsa.encrypt(message_num);
                        CHECK_THAT(
                                encrypted_num.to_string(16),
                                Catch::Equals(encrypted_str));
                    }
                }
            }
        }
    }

}

void correct_encryption_tests() {
    encryption_test("./RSA-Tests-2.json");
}

void wrong_encryption_tests() {
    encryption_test("./RSA-Tests-3.json");
}

TEMPLATE_TEST_CASE(
        "BigInt RSA extensions functionality check",
        "[bigint]",
        its::BigInt16, its::BigInt32, its::BigInt64) {

    egcd_tests<TestType>();
}

TEST_CASE(
        "RSA functionality tests",
        "[RSA]") {

    keygen_tests();
    correct_encryption_tests();
    wrong_encryption_tests();
}
