#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>

// this file should stay empty as it is just the general entry point for the
// catch2 execution. If at some point a more in depth test main is needed
// it can be added here, but should only be done if really required.