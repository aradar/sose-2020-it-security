#include <fstream>

#include <catch2/catch.hpp>
#include <nlohmann/json.hpp>

#include "bigint/BigInt.hpp"

using json = nlohmann::json;

template<class T>
void left_shift_tests() {
    std::ifstream input_stream("./Shift-Left-Tests_fixed.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto& element : json_obj) {
        SECTION(element["name"]) {
            auto cnt = element["start"].get<size_t>();
            auto increment = element["increment"].get<size_t>();
            for (auto& result : element["results"]) {
                T num(element["number"].get<std::string>());

                num.left_shift(cnt);
                CHECK_THAT(
                        num.to_string(16),
                        Catch::Equals(result.get<std::string>()));

                cnt += increment;
            }
        }
    }
}

template<class T>
void right_shift_tests() {
    std::ifstream input_stream("./Shift-Right-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto& element : json_obj) {
        SECTION(element["name"]) {
            auto cnt = element["start"].get<size_t>();
            auto increment = element["increment"].get<size_t>();
            for (auto& result : element["results"]) {
                T num(element["number"].get<std::string>());

                num.right_shift(cnt);
                CHECK_THAT(
                        num.to_string(16),
                        Catch::Equals(result.get<std::string>()));

                cnt += increment;
            }
        }
    }
}

template<class T>
void mul10_tests() {
    std::ifstream input_stream("./Mul10-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto& element : json_obj) {
        SECTION(element["name"]) {
            auto mul_exp = element["start"].get<size_t>();
            auto increment = element["increment"].get<size_t>();
            T num(element["number"].get<std::string>());
            T result;
            for (auto& expected_result : element["results"]) {
                auto mul_num = int(pow(10, mul_exp + 1));
                T multiplicand(std::to_string(mul_num));

                num.mul(multiplicand, result);
                CHECK_THAT(
                        result.to_string(16),
                        Catch::Equals(expected_result.get<std::string>()));

                mul_exp += increment;
            }
        }
    }
}

template<class T>
void div10_tests() {
    std::ifstream input_stream("./Div10-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto& element : json_obj) {
        SECTION(element["name"]) {
            auto div_exp = element["start"].get<size_t>();
            auto increment = element["increment"].get<size_t>();
            T dividend(element["number"].get<std::string>());
            T result;
            T remainder;
            for (auto& expected_result : element["results"]) {
                auto div_num = int(pow(10, div_exp + 1));
                T divisor(std::to_string(div_num));

                dividend.divmod(dividend, divisor, result, remainder, false);
                CHECK_THAT(
                        result.to_string(16),
                        Catch::Equals(expected_result.get<std::string>()));

                div_exp += increment;
            }
        }
    }
}

template<class T>
void to_string_tests() {
    std::ifstream input_stream("./Convert-Hex-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto& element : json_obj) {
        SECTION(element["name"]) {
            T num(element["number"].get<std::string>());
            CHECK_THAT(
                    num.to_string(element["out_num_base"]),
                    Catch::Equals(element["result"].get<std::string>()));
        }
    }
}

template<class T>
void arithmetic_tests() {
    std::ifstream input_stream("./Arithmetic-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto& element : json_obj) {
        SECTION(element["name"]) {
            T first_num(element["first_number"].get<std::string>());
            T second_num(element["second_number"].get<std::string>());

            T add_result;
            first_num.add(second_num, add_result);
            CHECK_THAT(
                    add_result.to_string(16),
                    Catch::Equals(element["add_result"].get<std::string>()));

            T sub_result;
            first_num.sub(second_num, sub_result);
            CHECK_THAT(
                    sub_result.to_string(16),
                    Catch::Equals(element["sub_result"].get<std::string>()));

            T mul_result;
            first_num.mul(second_num, mul_result);
            CHECK_THAT(
                    mul_result.to_string(16),
                    Catch::Equals(element["mul_result"].get<std::string>()));

            T div_result, mod_result;
            if (!element["div_result"].get<std::string>().empty()) {
                first_num.divmod(second_num, div_result, mod_result);
                CHECK_THAT(
                        div_result.to_string(16),
                        Catch::Equals(element["div_result"].get<std::string>()));
                CHECK_THAT(
                        mod_result.to_string(16),
                        Catch::Equals(element["mod_result"].get<std::string>()));
            } else{
                CHECK_THROWS(first_num.divmod(second_num, div_result, mod_result));
            }
        }
    }
}

template<class T>
void compare_tests() {
    std::ifstream input_stream("./Compare-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto& element : json_obj) {
        SECTION(element["name"]) {
            T first_num(element["first_number"].get<std::string>());
            T second_num(element["second_number"].get<std::string>());

            CHECK(first_num.comp(second_num) == element["first_sec_comp"].get<int>());
            CHECK(second_num.comp(first_num) == element["sec_first_comp"].get<int>());
        }
    }
}

TEMPLATE_TEST_CASE(
        "Basic functionality check of BigInt",
        "[bigint]",
        its::BigInt16, its::BigInt32, its::BigInt64) {

    left_shift_tests<TestType>();
    right_shift_tests<TestType>();
    mul10_tests<TestType>();
    div10_tests<TestType>();
    to_string_tests<TestType>();
    arithmetic_tests<TestType>();
    compare_tests<TestType>();
}
