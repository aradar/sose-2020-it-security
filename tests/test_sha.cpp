#include <fstream>

#include <catch2/catch.hpp>
#include <nlohmann/json.hpp>

#include <sha/Sha256.hpp>

using json = nlohmann::json;

TEST_CASE(
        "sha256 digest tests",
        "[sha256]") {

    std::ifstream input_stream("./sha256-tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        SECTION(element["name"]) {
            auto message = element["message"].get<std::string>();

            SECTION("direct digest") {
                its::Sha256 sha;
                sha.hexupdate(message);

                auto direct_digest = element["direct_digest"].get<std::string>();
                direct_digest.erase(0, 3);
                CHECK_THAT(
                        sha.hexdigest(),
                        Catch::Equals(direct_digest));
            }
            SECTION("double digest") {
                its::Sha256 sha;
                sha.hexupdate(message);
                auto first_round_digest = sha.hexdigest();

                sha = its::Sha256();
                sha.hexupdate(first_round_digest);

                auto double_digest = element["double_digest"].get<std::string>();
                double_digest.erase(0, 3);
                CHECK_THAT(
                        sha.hexdigest(),
                        Catch::Equals(double_digest));
            }
        }
    }
}
