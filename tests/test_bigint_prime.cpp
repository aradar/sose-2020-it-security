#include <fstream>

#include <catch2/catch.hpp>
#include <nlohmann/json.hpp>

#include "bigint/BigInt.hpp"

using json = nlohmann::json;

template<class T>
void square_tests() {
    std::ifstream input_stream("./Square-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        SECTION(element["name"]) {
            T num(element["number"].get<std::string>());
            for (auto &result : element["results"]) {
                num.square();
                CHECK_THAT(
                        num.to_string(16),
                        Catch::Equals(result.get<std::string>()));
            }
        }
    }
}

template<class T>
void pow_big_tests() {
    std::ifstream input_stream("./Power-Big-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        SECTION(element["name"]) {
            T base(element["first_number"].get<std::string>());
            T exp(element["second_number"].get<std::string>());
            base.pow(exp);
            CHECK_THAT(
                    base.to_string(16),
                    Catch::Equals(element["result"].get<std::string>()));
        }
    }
}

template<class T>
void pow_small_tests() {
    std::ifstream input_stream("./Power-Small-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        SECTION(element["name"]) {
            T num(element["number"].get<std::string>());
            auto exp = element["start"].get<size_t>();
            auto increment = element["increment"].get<size_t>();
            for (auto &expected_result : element["results"]) {
                T exp_num(std::to_string(exp));
                T result;
                num.pow(exp_num, result);
                CHECK_THAT(
                        result.to_string(16),
                        Catch::Equals(expected_result.get<std::string>()));
                exp += increment;
            }
        }
    }
}

template<class T>
void mod_pow_tests() {
    std::ifstream input_stream("./PowerMod-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        SECTION(name) {
            T base(element["first_number"].get<std::string>());
            T exp(element["second_number"].get<std::string>());
            T module(element["module"].get<std::string>());

            base.pow_mod(exp, module);
            CHECK_THAT(
                    base.to_string(16),
                    Catch::Equals(element["result"].get<std::string>()));
        }
    }
}

template<class T>
void mod_pow_prime_tests() {
    std::ifstream input_stream("./PowerMod-Prim-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        SECTION(name) {
            T base(element["first_number"].get<std::string>());
            T exp(element["second_number"].get<std::string>());
            T module(element["module"].get<std::string>());

            base.pow_mod(exp, module);
            CHECK_THAT(
                    base.to_string(16),
                    Catch::Equals(element["result"].get<std::string>()));
        }
    }
}

template<class T>
void euler_pseudo_prime_tests() {
    std::ifstream input_stream("./EulerPseudoPrime-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        SECTION(name) {
            T prime(element["prime_num"].get<std::string>());
            T base(element["base_num"].get<std::string>());

            CHECK(prime.is_euler_prime(base));
        }
    }
}

template<class T>
void fermat_pseudo_prime_tests() {
    std::ifstream input_stream("./FermatPseudoPrime-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        SECTION(name) {
            T prime(element["prime_num"].get<std::string>());
            T base(element["base_num"].get<std::string>());

            CHECK(prime.is_fermat_prime(base, false));
        }
    }
}

template<class T>
void prime_number_tests() {
    std::ifstream input_stream("./primeNumber-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        auto prime_str = element["prime_num"].get<std::string>();
        SECTION(name + ": (" + prime_str + ")") {
            for (auto &base_element : element["bases"]) {
                auto base_str = base_element.get<std::string>();
                SECTION("for base: " + base_str) {
                    T prime(prime_str);
                    T base(base_str);

                    SECTION("is_fermat_prime") {
                        CHECK(prime.is_fermat_prime(base));
                    }
                    SECTION("is_euler_prime") {
                        CHECK(prime.is_euler_prime(base));
                    }
                    SECTION("is_mr_prime") {
                        CHECK(prime.is_mr_prime(base));
                    }
                }
            }
        }
    }
}

template<class T>
void no_prime_number_tests() {
    std::ifstream input_stream("./NoPrimeNumber-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        auto prime_str = element["prime_num"].get<std::string>();
        auto base_str = element["base_num"].get<std::string>();
        SECTION(name + " with prime (" + prime_str + ") and base (" + base_str + ")") {
            T prime(prime_str);
            T base(base_str);

            SECTION("is_euler_prime") {
                bool is_euler_prime = element["is_euler_prime"].get<bool>();
                CHECK(prime.is_euler_prime(base) == is_euler_prime);
            }

            SECTION("is_fermat_prime") {
                bool is_fermat_prime = element["is_fermat_prime"].get<bool>();
                CHECK(prime.is_fermat_prime(base) == is_fermat_prime);
            }

            SECTION("is_mr_prime") {
                CHECK(!prime.is_mr_prime(base));
            }
        }
    }
}

template<class T>
void gcd_tests() {
    std::ifstream input_stream("./gcd-Tests.json");
    json json_obj;
    input_stream >> json_obj;

    for (auto &element : json_obj) {
        auto name = element["name"].get<std::string>();
        SECTION(name) {
            T first(element["first_num"].get<std::string>());
            T second(element["second_num"].get<std::string>());

            T gcd_result;
            gcd_result.gcd_bin(first, second);
            CHECK_THAT(
                    gcd_result.to_string(16),
                    Catch::Equals(element["gcd_num"].get<std::string>()));
        }
    }
}

TEMPLATE_TEST_CASE(
        "Extended functionality check for prime numbers of BigInt",
        "[bigint]",
        its::BigInt16, its::BigInt32, its::BigInt64) {

    square_tests<TestType>();
    pow_small_tests<TestType>();
    pow_big_tests<TestType>();
    mod_pow_tests<TestType>();
    mod_pow_prime_tests<TestType>();
    euler_pseudo_prime_tests<TestType>();
    fermat_pseudo_prime_tests<TestType>();
    prime_number_tests<TestType>();
    no_prime_number_tests<TestType>();
    gcd_tests<TestType>();
}
