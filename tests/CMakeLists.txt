FetchContent_Declare(
    catch2
    GIT_REPOSITORY https://github.com/catchorg/Catch2.git
    GIT_TAG        v2.11.3
)
FetchContent_MakeAvailable(catch2)

FetchContent_Declare(json
    GIT_REPOSITORY https://github.com/nlohmann/json.git
    GIT_TAG v3.7.3)
FetchContent_MakeAvailable(json)

add_executable(its_testlib
    main.cpp
    test_bigint_basics.cpp
    test_bigint_prime.cpp
    test_sha.cpp)
target_link_libraries(its_testlib PRIVATE Catch2::Catch2 nlohmann_json::nlohmann_json bigint)

add_executable(its_testlib_bigint
    main.cpp
    test_bigint_basics.cpp)
target_link_libraries(its_testlib_bigint PRIVATE Catch2::Catch2 nlohmann_json::nlohmann_json bigint)

add_executable(its_testlib_prime
    main.cpp
    test_bigint_prime.cpp)
target_link_libraries(its_testlib_prime PRIVATE Catch2::Catch2 nlohmann_json::nlohmann_json bigint)

add_executable(its_testlib_rsa
    main.cpp
    test_rsa.cpp)
target_link_libraries(its_testlib_rsa PRIVATE Catch2::Catch2 nlohmann_json::nlohmann_json bigint rsa_lib)

add_executable(its_testlib_sha
    main.cpp
    test_sha.cpp)
target_link_libraries(its_testlib_sha PRIVATE Catch2::Catch2 nlohmann_json::nlohmann_json hash_lib)

configure_file(./bigint_basics_case_files/Convert-Hex-Tests.json Convert-Hex-Tests.json COPYONLY)
configure_file(./bigint_basics_case_files/Arithmetic-Tests.json Arithmetic-Tests.json COPYONLY)
configure_file(./bigint_basics_case_files/Div10-Tests.json Div10-Tests.json COPYONLY)
configure_file(./bigint_basics_case_files/Mul10-Tests.json Mul10-Tests.json COPYONLY)
configure_file(./bigint_basics_case_files/Shift-Left-Tests_fixed.json Shift-Left-Tests_fixed.json COPYONLY)
configure_file(./bigint_basics_case_files/Shift-Right-Tests.json Shift-Right-Tests.json COPYONLY)
configure_file(./bigint_basics_case_files/Compare-Tests.json Compare-Tests.json COPYONLY)

configure_file(./bigint_prime_case_files/Square-Tests.json Square-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/PowerMod-Tests.json PowerMod-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/PowerMod-Prim-Tests.json PowerMod-Prim-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/Power-Small-Tests.json Power-Small-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/Power-Big-Tests.json Power-Big-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/EulerPseudoPrime-Tests.json EulerPseudoPrime-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/FermatPseudoPrime-Tests.json FermatPseudoPrime-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/NoPrimeNumber-Tests.json NoPrimeNumber-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/primeNumber-Tests.json primeNumber-Tests.json COPYONLY)
configure_file(./bigint_prime_case_files/gcd-Tests.json gcd-Tests.json COPYONLY)

configure_file(./rsa_case_files/egcd-Tests.json egcd-Tests.json COPYONLY)
configure_file(./rsa_case_files/RSA-Tests-1.json RSA-Tests-1.json COPYONLY)
configure_file(./rsa_case_files/RSA-Tests-2.json RSA-Tests-2.json COPYONLY)
configure_file(./rsa_case_files/RSA-Tests-3.json RSA-Tests-3.json COPYONLY)

configure_file(./sha_case_files/sha256-tests.json sha256-tests.json COPYONLY)
