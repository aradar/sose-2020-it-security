#!/usr/bin/env python3

import argparse
from pathlib import Path
from typing import List, Dict
import json


def shorten_hex(hex_str: str) -> str:
    if not hex_str:
        return hex_str

    prefix = ""
    if hex_str.startswith("+") or hex_str.startswith("-"):
        prefix = hex_str[:3]
        hex_str = hex_str[3:]
    else:
        prefix = hex_str[:2]
        hex_str = hex_str[2:]

    hex_str = hex_str.lstrip("0")

    if not hex_str:
        hex_str = "0"

    return prefix + hex_str


def interpret_linear_inc(
        test_dicts: List[Dict[str, str]],
        start: int,
        increment: int,
        changed_result_keys: List[str],
        **kwargs) \
        -> List[Dict[str, str]]:

    name_key = "t"
    initial_num_key = "a"
    result_num_keys = ["b", "c", "d", "e", "f", "g", "h"]

    if changed_result_keys:
        result_num_keys = changed_result_keys

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["style"] = "linear_inc"
        curr_dict["start"] = start
        curr_dict["increment"] = increment
        curr_dict["number"] = shorten_hex(old_dict[initial_num_key])

        result_list = list()
        curr_dict["results"] = result_list
        for key in result_num_keys:
            result_list.append(shorten_hex(old_dict[key]))

    return new_dicts


def interpret_comp(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    num_keys = ["a", "b"]
    first_sec_comp_key = "c"
    sec_first_comp_key = "d"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["first_number"] = shorten_hex(old_dict[num_keys[0]])
        curr_dict["second_number"] = shorten_hex(old_dict[num_keys[1]])

        curr_dict["first_sec_comp"] = int(old_dict[first_sec_comp_key])
        curr_dict["sec_first_comp"] = int(old_dict[sec_first_comp_key])

    return new_dicts


def interpret_to_string(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    num_key = "d"
    result_num_keys = ["d", "h", "o"]

    new_dicts = list()
    for old_dict in test_dicts:
        for curr_result_key in result_num_keys:
            curr_dict = dict()
            new_dicts.append(curr_dict)

            curr_dict["number"] = old_dict[num_key]
            curr_dict["result"] = old_dict[curr_result_key]
            curr_dict["name"] = old_dict[name_key]

            if curr_result_key == "d":
                curr_dict["name"] = old_dict[name_key].replace("Hex", "Dec")
                curr_dict["out_num_base"] = 10
            elif curr_result_key == "o":
                curr_dict["name"] = old_dict[name_key].replace("Hex", "Oct")
                curr_dict["out_num_base"] = 8
            elif curr_result_key == "h":
                curr_dict["result"] = shorten_hex(old_dict[curr_result_key])
                curr_dict["out_num_base"] = 16

    return new_dicts


def interpret_arithmetics(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    num_keys = ["a", "b"]
    add_result_key = "+"
    sub_result_key = "-"
    mul_result_key = "*"
    div_result_key = "/"
    mod_result_key = "%"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["first_number"] = shorten_hex(old_dict[num_keys[0]])
        curr_dict["second_number"] = shorten_hex(old_dict[num_keys[1]])

        curr_dict["add_result"] = shorten_hex(old_dict[add_result_key])
        curr_dict["sub_result"] = shorten_hex(old_dict[sub_result_key])
        curr_dict["mul_result"] = shorten_hex(old_dict[mul_result_key])
        curr_dict["div_result"] = shorten_hex(old_dict[div_result_key])
        curr_dict["mod_result"] = shorten_hex(old_dict[mod_result_key])

    return new_dicts


def interpret_arithmetics(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    num_keys = ["a", "b"]
    add_result_key = "+"
    sub_result_key = "-"
    mul_result_key = "*"
    div_result_key = "/"
    mod_result_key = "%"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["first_number"] = shorten_hex(old_dict[num_keys[0]])
        curr_dict["second_number"] = shorten_hex(old_dict[num_keys[1]])

        curr_dict["add_result"] = shorten_hex(old_dict[add_result_key])
        curr_dict["sub_result"] = shorten_hex(old_dict[sub_result_key])
        curr_dict["mul_result"] = shorten_hex(old_dict[mul_result_key])
        curr_dict["div_result"] = shorten_hex(old_dict[div_result_key])
        curr_dict["mod_result"] = shorten_hex(old_dict[mod_result_key])

    return new_dicts


def interpret_power(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    num_keys = ["a", "b"]
    result_key = "c"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["first_number"] = shorten_hex(old_dict[num_keys[0]])
        curr_dict["second_number"] = shorten_hex(old_dict[num_keys[1]])

        curr_dict["result"] = shorten_hex(old_dict[result_key])

    return new_dicts


def interpret_powermod(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    num_keys = ["a", "b"]
    module_key = "m"
    result_key = "c"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["first_number"] = shorten_hex(old_dict[num_keys[0]])
        curr_dict["second_number"] = shorten_hex(old_dict[num_keys[1]])
        curr_dict["module"] = shorten_hex(old_dict[module_key])
        curr_dict["result"] = shorten_hex(old_dict[result_key])

    return new_dicts


def interpret_prime(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    basis_key = "a"
    prime_key = "p"
    euler_optional_key = "e"
    fermat_optional_key = "f"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["prime_num"] = shorten_hex(old_dict[prime_key])

        if isinstance(old_dict[basis_key], list):
            curr_dict["bases"] = [shorten_hex(b) for b in old_dict[basis_key]]
        else:
            curr_dict["base_num"] = shorten_hex(old_dict[basis_key])

        if old_dict.get(euler_optional_key):
            curr_dict["is_euler_prime"] = old_dict[euler_optional_key].lower() == "true"
        if old_dict.get(fermat_optional_key):
            curr_dict["is_fermat_prime"] = old_dict[euler_optional_key].lower() == "true"

    return new_dicts


def interpret_gcd(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    first_num_key = "a"
    second_num_key = "b"
    gcd_key = "g"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["first_num"] = shorten_hex(old_dict[first_num_key])
        curr_dict["second_num"] = shorten_hex(old_dict[second_num_key])
        curr_dict["gcd_num"] = shorten_hex(old_dict[gcd_key])

    return new_dicts


def interpret_sha(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:

    # t is the title of the test
    # p message in hex
    # l is length of block in bit (decimal)
    # k is length of 0 behind block excluding 64 bit length and 1
    #    last block is l+0x80+..+64 bit size (padding)
    #    k is the count of 0 between 1 and size (decimal)
    # H-array is H[0..7] in blocks of 4 byte in Hex with a blank
    # W-array is W[0..7] at start in blocks of 4 byte in Hex with a blank
    # R[Nr] is A,B,C,D,E,F,G as 4 byte in Hex with a blank at round Nr
    # h sha-256 in hex
    # same as above
    # d sha-256(sha-256) in hex

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict["t"]
        curr_dict["message"] = "+0x" + old_dict["p"]
        curr_dict["direct_digest"] = "+0x" + old_dict["h"]  # normal hash
        curr_dict["double_digest"] = old_dict["d"]  # hash of the digest

        round_data = dict()
        curr_dict["debug_data"] = round_data
        for r_id, debug_key in enumerate(["direct", "double"]):
            round_dict = dict()
            round_data[debug_key] = round_dict

            round_dict["bit_block_size"] = old_dict["l"][r_id]
            round_dict["num_padding_bits"] = old_dict["k"][r_id]
            round_dict["h"] = old_dict["H"][r_id]
            round_dict["w"] = old_dict["W"][r_id]

            registers = list()
            round_dict["register_values"] = registers
            for i in range(64):
                registers.append(old_dict["R[{:02d}]".format(i)][r_id])

    return new_dicts


def interpret_egcd(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    first_num_key = "a"
    second_num_key = "b"
    gcd_key = "g"
    u_key = "u"
    v_key = "v"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["first_num"] = shorten_hex(old_dict[first_num_key])
        curr_dict["second_num"] = shorten_hex(old_dict[second_num_key])
        curr_dict["gcd_num"] = shorten_hex(old_dict[gcd_key])
        curr_dict["u_num"] = shorten_hex(old_dict[u_key])
        curr_dict["v_num"] = shorten_hex(old_dict[v_key])

    return new_dicts


def interpret_rsa_keygen(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    p_key = "p"
    q_key = "q"
    e_key = "e"
    phi_key = "f"
    d_key = "d"

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["p"] = shorten_hex(old_dict[p_key])
        curr_dict["q"] = shorten_hex(old_dict[q_key])
        curr_dict["e"] = shorten_hex(old_dict[e_key])
        curr_dict["to_find_phi"] = shorten_hex(old_dict[phi_key])
        curr_dict["to_find_d"] = shorten_hex(old_dict[d_key])

    return new_dicts


def interpret_rsa_encryption(test_dicts: List[Dict[str, str]], **kwargs) -> List[Dict[str, str]]:
    name_key = "t"
    p_key = "p"
    q_key = "q"
    e_key = "e"
    d_key = "d"
    message_keys = ["g", "i", "k"]
    encrypted_message_keys = ["h", "j", "l"]

    new_dicts = list()
    for old_dict in test_dicts:
        curr_dict = dict()
        new_dicts.append(curr_dict)

        curr_dict["name"] = old_dict[name_key]
        curr_dict["p"] = shorten_hex(old_dict[p_key])
        curr_dict["q"] = shorten_hex(old_dict[q_key])
        curr_dict["e"] = shorten_hex(old_dict[e_key])
        curr_dict["calculated_d"] = shorten_hex(old_dict[d_key])

        messages = list()
        curr_dict["messages"] = messages
        for m_key, em_key in zip(message_keys, encrypted_message_keys):
            mess_dict = dict()
            messages.append(mess_dict)
            mess_dict["raw"] = shorten_hex(old_dict[m_key])
            mess_dict["encrypted"] = shorten_hex(old_dict[em_key])

    return new_dicts


interpretation_style_map = {
    "linear_inc": interpret_linear_inc,
    "comp": interpret_comp,
    "to_string": interpret_to_string,
    "arithmetics": interpret_arithmetics,
    "power": interpret_power,
    "powermod": interpret_powermod,
    "prime": interpret_prime,
    "gcd": interpret_gcd,
    "sha": interpret_sha,
    "egcd": interpret_egcd,
    "rsa_keygen": interpret_rsa_keygen,
    "rsa_encryption": interpret_rsa_encryption
}


def extract_test_line_blocks(lines: List[str]):
    blocks = list()
    for line_no, line in enumerate(lines):
        stripped_line = line.strip()

        if stripped_line.startswith("#"):
            continue  # skip comments

        if not blocks and not line.startswith("t"):
            print("Line number {} seems to be not in a test block. Skipping it".format(line_no + 1))

        if line.startswith("t"):  # new test block
            blocks.append(list())
        blocks[-1].append(stripped_line)

    return blocks


def simple_test_dict(blocks: List[List[str]]) -> List[Dict[str, str]]:
    test_dicts = list()
    for curr_block in blocks:
        curr_dict = dict()
        test_dicts.append(curr_dict)
        for element in curr_block:
            if element:
                key, value = [e.strip() for e in element.split("=", 1)]

                if key in curr_dict:
                    key_el = curr_dict[key]

                    if not isinstance(key_el, list):
                        curr_dict[key] = [key_el]

                    curr_dict[key].append(value)
                else:
                    curr_dict[key] = value

    return test_dicts


def interpret_simple_test_dicts(
        test_dicts: List[Dict[str, str]],
        style: str, **kwargs) \
        -> List[Dict[str, str]]:

    conv_func = interpretation_style_map[style]
    return conv_func(test_dicts, **kwargs)


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("TEST_FILE", type=Path)
    parser.add_argument(
        "INTERPRETATION_STYLE", type=str,
        choices=list(interpretation_style_map.keys()))
    parser.add_argument("--start", type=int, default=1)
    parser.add_argument("--increment", type=int, default=1)
    parser.add_argument("--result-keys", nargs="+", type=str, default="")

    args = parser.parse_args()

    test_file = args.TEST_FILE  # type: Path
    style = args.INTERPRETATION_STYLE  # type: str

    interpretation_kwargs = {
        "start": args.start,
        "increment": args.increment,
        "changed_result_keys": args.result_keys
    }

    if not (test_file.exists() and test_file.is_file()):
        parser.print_help()
        exit(-1)

    with test_file.open("r") as f:
        lines = f.readlines()
        test_blocks = extract_test_line_blocks(lines)
        test_dicts = simple_test_dict(test_blocks)
        reformatted_test_dicts = interpret_simple_test_dicts(test_dicts, style, **interpretation_kwargs)

        out_file = test_file.with_suffix(".json")
        with out_file.open("w") as o:
            json.dump(reformatted_test_dicts, o, indent=4)


if __name__ == '__main__':
    main()
