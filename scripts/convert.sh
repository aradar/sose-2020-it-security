#!/usr/bin/env bash

# exercise 1
./tests_to_json.py test_files/Compare-Tests.txt comp
./tests_to_json.py test_files/Arithmetic-Tests.txt arithmetics
./tests_to_json.py test_files/Convert-Hex-Tests.txt to_string
./tests_to_json.py test_files/Div10-Tests.txt linear_inc --start 0
./tests_to_json.py test_files/Mul10-Tests.txt linear_inc --start 0
./tests_to_json.py test_files/Shift-Left-Tests_fixed.txt linear_inc
./tests_to_json.py test_files/Shift-Right-Tests.txt linear_inc

# exercise 2
./tests_to_json.py test_files/Square-Tests.txt linear_inc --start 1 --increment 1
./tests_to_json.py test_files/Power-Small-Tests.txt linear_inc --start 0 --increment 1 --result-keys b c d e f g h i
./tests_to_json.py test_files/Power-Big-Tests.txt power
./tests_to_json.py test_files/PowerMod-Tests.txt powermod
./tests_to_json.py test_files/PowerMod-Prim-Tests.txt powermod
./tests_to_json.py test_files/EulerPseudoPrime-Tests.txt prime
./tests_to_json.py test_files/FermatPseudoPrime-Tests.txt prime
./tests_to_json.py test_files/NoPrimeNumber-Tests.txt prime
./tests_to_json.py test_files/primeNumber-Tests.txt prime
./tests_to_json.py test_files/gcd-Tests.txt gcd

# exercise 3
./tests_to_json.py test_files/egcd-Tests.txt egcd
./tests_to_json.py test_files/RSA-Tests-1.txt rsa_keygen
./tests_to_json.py test_files/RSA-Tests-2.txt rsa_encryption
./tests_to_json.py test_files/RSA-Tests-3.txt rsa_encryption

# exercise 5
./tests_to_json.py test_files/sha256-tests.txt sha
