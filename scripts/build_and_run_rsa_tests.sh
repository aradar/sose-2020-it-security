#!/usr/bin/env bash

# ---- config start ----
BUILD_TARGET=its_testlib_rsa
REL_BUILD_DIR=rsa-tests-build-release
# ---- config end ----

SCRIPT_PATH="$( cd "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"
PROJECT_ROOT=$(realpath $SCRIPT_PATH/..)
BUILD_DIR=$PROJECT_ROOT/$REL_BUILD_DIR

cd $PROJECT_ROOT
echo "--------------------------------------------------------------------------------"
echo "building project"
echo "--------------------------------------------------------------------------------"

if [ ! -d "$BUILD_DIR" ]
then
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR
    cmake \
        -DCMAKE_BUILD_TYPE=Release \
        $PROJECT_ROOT
    if [ $? -ne 0 ]; then
        echo ""
        echo "project configuration failed! see the error above for more information."
        exit $?
    fi
    cmake --build . --target $BUILD_TARGET -- -j$(nproc --all)
    if [ $? -ne 0 ]; then
        echo ""
        echo "project building failed! see the error above for more information."
        exit $?
    fi
else
    echo "found old build. reusing it (if this is not desired delete $BUILD_DIR and $INSTALL_DIR"
fi

echo "--------------------------------------------------------------------------------"
echo "running tests"
echo "--------------------------------------------------------------------------------"

cd $BUILD_DIR/tests/
./$BUILD_TARGET --success
